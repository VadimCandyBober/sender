﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dropbox.Api.TeamLog;
using UnityEngine;
using UnityEngine.UI;

public class VersionController : MonoBehaviour
{
   [SerializeField] private Text _versionText;

   private void Awake()
   {
      _versionText.text = "Version " + Application.version;
   }
}
