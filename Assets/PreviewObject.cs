﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Dropbox.Api.Files;
using Telegram.Bot.Types;
using UnityEngine;
using UnityEngine.UI;

public class PreviewObject : MonoBehaviour
{
   public Image _previewImage;

    public bool _toogle;

    public string _path;

    [HideInInspector]public GameObject _toogleObj;

    [HideInInspector]public PreviewObject _lastPreviewObject;

    private Sprite _image;

    public bool _can = true;

    private void Update()
    {
        _previewImage.rectTransform.sizeDelta = GetComponent<RectTransform>().sizeDelta;
    }

    public void SetImage(Sprite prevImage, string path, Sprite customImage = null)
    {
        _previewImage.sprite = prevImage;

        _path = path;

        _image = customImage;
    }
    
    public void SetImageToPreview(Sprite prevImage, string path,bool needScale = false, Sprite customImage = null)
    {
        _previewImage.sprite = prevImage;

        _path = path;

        _image = customImage;

        if (needScale)
        {
            _previewImage.transform.localScale = new Vector3( transform.localScale .x, transform.localScale .y * -1, transform.localScale.z);
        }
    }
    
    

    public void OpenVideo()
    {
        if (_can)
        {
            string format = Path.GetExtension(_path);
            Debug.Log(format);
            if(format == ".mp4" || format == ".mov" || format == ".MP4" || format == ".MOV")
                FindObjectOfType<VideoPlayManager>().Open(_path, _previewImage.sprite, this);
            else if (format == ".gif" || format == ".GIF")
            {
                FindObjectOfType<VideoPlayManager>().OpenGif(_path, _previewImage.sprite, this, _image);
            }
            else
            {
                FindObjectOfType<VideoPlayManager>().OpenImage(_path, _previewImage.sprite, this, _image);
            }
        }
        
    }

    public void ToogleChanged()
    {
        if (AddVideoToSendList._instance.CanAdd())
        {
            _toogle = !_toogle;
        
            Debug.Log(_toogle);
        
            if(_toogle)
                AddVideoToSendList._instance.Add(_path, _previewImage.sprite, this, -1);
            else AddVideoToSendList._instance.RemoveFileToSend(_path);
        
            _toogleObj.SetActive(_toogle);
        }
        else
        {
            if (_toogle)
            {
                _toogle = !_toogle;
                AddVideoToSendList._instance.RemoveFileToSend(_path);
            }
        }
       
        
    }
    
    public void Off()
    {
        _toogle = false;
        _toogleObj.SetActive(false);
    }
    
    
    
    
}
