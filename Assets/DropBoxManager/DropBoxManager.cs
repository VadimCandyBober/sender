﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Crosstales.FB;
using Dropbox.Api;
using Dropbox.Api.FileProperties;
using Dropbox.Api.Files;
using Dropbox.Api.Sharing;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class DropBoxManager : MonoBehaviour
{

   [SerializeField] private Text _pathText;

   [SerializeField] private Text _credPath;
   
   [SerializeField] private Text _credPathCuet;

   [SerializeField] private Text _credPathMail;
   
   [SerializeField] private Text _credPathSms;
   
   private DropBoxData _dropBoxData;

   public static DropBoxManager _instance;

   private DropBoxSendedFiles _dropBoxSended;
   
   

   private void Awake()
   {
      DontDestroyOnLoad(gameObject);
      
      if (_instance == null)
         _instance = this;
      else
      {
          Destroy(gameObject);
      }
   }

   public string GetPath()
   {
      if (_dropBoxData != null)
         return _dropBoxData._credPath;
      return "-1";
   }
  

   public async void UpdateDropBox()
   {
      try
      {
         bool dropBoxIsActive = SendKindsDataBase._instance.GetDropBox();

         if (dropBoxIsActive && _dropBoxData._path != null)
         {
            var ext = new List<string> {".jpg", ".png", ".mp4", ".mov", ".JPG", ".PNG", ".MP4", ".MOV", ".gif", ".GIF", ".gif"};
         
            var movFilesSync = Directory.GetFiles(_dropBoxData._path, "*.*")
               .Where(s => ext.Contains(Path.GetExtension(s))).ToList();

            foreach (var file in movFilesSync)
            {

               if (!_dropBoxSended._sendedFiles.Contains(file))
               {
                  string fileName = Path.GetFileName(file);
                  
                  string res = await GoogleDriveTest._instance.GoogleDriveUpload(file,false);

                  await Task.Delay(100);
               
                  if(res != "-1")
                     _dropBoxSended._sendedFiles.Add(file);
            
                  SaveSendedFiles();
               }
            }
       
         }

         await System.Threading.Tasks.Task.Delay(10000);
      
         UpdateDropBox();
      }
      catch (Exception e)
      {
         Debug.Log(e);
      }
   
   }

   private void Start()
   {
      _dropBoxSended = GetSendedFiles();
      
      var json = PlayerPrefs.GetString("DropBoxData", "");
      
      if (json != "")
      {
         _dropBoxData = JsonUtility.FromJson<DropBoxData>(json);
         _pathText.text = _dropBoxData._path;
         UIUpdate();
      }
      else
      {
         _dropBoxData = new DropBoxData();
        
      }
      
      UpdateDropBox();

   }

   void UIUpdate()
   {
      _pathText.text = _dropBoxData._path;
      _credPath.text = _dropBoxData._credPath;
      _credPathCuet.text = Path.GetFileName(_dropBoxData._credPath);
      _credPathMail.text = Path.GetFileName(_dropBoxData._credPath);
      _credPathSms.text = Path.GetFileName(_dropBoxData._credPath);
      
   }

   public void PathSelect()
   {
      _dropBoxData._path =FileBrowser.OpenSingleFolder("Select folder");
      Save();
      UIUpdate();
   }
   
   public void CredPath()
   {
      _dropBoxData._credPath = FileBrowser.OpenSingleFile();
      Save();
      UIUpdate();
   }

   public void SetApiKey()
   {
      Save();
      UIUpdate();
   }
   
   private void Save()
   {
      PlayerPrefs.SetString("DropBoxData", JsonUtility.ToJson(_dropBoxData));
      PlayerPrefs.Save();
   }

   private void SaveSendedFiles()
   {
      PlayerPrefs.SetString("SendedFilesDropBox", JsonUtility.ToJson(_dropBoxSended));
      PlayerPrefs.Save();
   }

   private DropBoxSendedFiles GetSendedFiles()
   {
      var json = PlayerPrefs.GetString("SendedFilesDropBox", "");

      if (json != "")
      {
         return JsonUtility.FromJson<DropBoxSendedFiles>(json);
      } 
      return  new DropBoxSendedFiles();
   }
   
 
   
}

[System.Serializable]
public class DropBoxData
{
   public string _credPath;
   
   public string _path;
}

[System.Serializable]
public class DropBoxSendedFiles
{
   public List<string> _sendedFiles = new List<string>();
}
