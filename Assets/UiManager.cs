﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiManager : MonoBehaviour
{
    [SerializeField] private GameObject _mainTab;
    
    [SerializeField] private GameObject _hotFoldersTab;
    
    [SerializeField] private GameObject _sendMethodsTab;
    
    [SerializeField] private GameObject _uiCustomizeTab;
    
    [SerializeField] private GameObject _licenseTab;

    [SerializeField] private GameObject _senderListTab;

    [SerializeField] private GameObject _contentSaveTab;

    private void Awake()
    {
        OffAllTabs();
        _mainTab.SetActive(true);
    }

    private void OffAllTabs()
    {
        _mainTab.SetActive(false);
        _hotFoldersTab.SetActive(false);
        _sendMethodsTab.SetActive(false);
        _uiCustomizeTab.SetActive(false);
        _licenseTab.SetActive(false);
        _senderListTab.SetActive(false);
        _contentSaveTab.SetActive(false);
    }

    public void OpenMainTab()
    {
        OffAllTabs();
        _mainTab.SetActive(true);
    }

    public void OpenHotFolderTab()
    {
        OffAllTabs();
        _hotFoldersTab.SetActive(true);
    }

    public void OpenSendMethodsTab()
    {
        OffAllTabs();
        _sendMethodsTab.SetActive(true);
    }

    public void OpenUiCustomizeTab()
    {
        OffAllTabs();
        _uiCustomizeTab.SetActive(true);
    }

    public void OpenLicenseTab()
    {
        OffAllTabs();
        _licenseTab.SetActive(true);
        FindObjectOfType<LicenseActivate>().GoToACtivationTab();
    } 
    
    public void OpenContentSave()
    {
        OffAllTabs();
        _contentSaveTab.SetActive(true);
    } 
    
    public void OpenListTab()
    {
        OffAllTabs();
        _senderListTab.SetActive(true);
    } 
}
