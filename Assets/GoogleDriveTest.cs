﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using UnityEngine.UI;
using File = Google.Apis.Drive.v3.Data.File;
using Random = UnityEngine.Random;

public class GoogleDriveTest : MonoBehaviour
{
    public static GoogleDriveTest _instance;

    UserCredential credential;

    private GoogleDrivesFolderNames _folders;

    [SerializeField] private InputField _isntFolderInput;
    
    [SerializeField] private InputField _mailFolderInput;

    [SerializeField] private InputField _syncFolderInput;
    
    [SerializeField] private InputField _smsFolderInput;

    [SerializeField] private GameObject _goodResultInst;
    
    [SerializeField] private GameObject _badResultInst;
    
    [SerializeField] private GameObject _goodResultSync;
    
    [SerializeField] private GameObject _badResultSync;
    
    [SerializeField] private GameObject _goodResultMail;
    
    [SerializeField] private GameObject _badResultMail;

    [SerializeField] private Text _freeSpaceText;
    
    //-------------------------------------------------

    [SerializeField] private GameObject _instSelctFolderError;
    
    [SerializeField] private GameObject _mailSelctFolderError;
    
    [SerializeField] private GameObject _smsSelctFolderError;
    
    [SerializeField] private GameObject _syncSelctFolderError;
    
    //------------------------------------------------- sms
    
    [SerializeField] private GameObject _goodResultSms;
    
    [SerializeField] private GameObject _badResultSms;
    
    
    
    
    private DropBoxManager _dropBoxManager;
    
    private void Awake()
    {
        _dropBoxManager = FindObjectOfType<DropBoxManager>();
        _instance = this;

        string json = PlayerPrefs.GetString("Folders", "-1");

        if (json != "-1")
        {
            _folders = JsonUtility.FromJson<GoogleDrivesFolderNames>(json);
        }
        else
        {
            _folders = new GoogleDrivesFolderNames();
        }
        
        UiUpdate();
        
        StartCoroutine(AboutUpdate());
    }

    private void Start()
    {
        Debug.Log("START");
        if (LocalizationManager._instance.Language == Language.en)
        {
            _freeSpaceText.text = "Loading data ";
        }
        else
        {
            _freeSpaceText.text = "Загрузка данных ";
        }

    }

    public void DeleteCreditionals()
    {
        try
        {
            System.IO.Directory.Delete("token.json",true);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    private void Update()
    {
        
    }

    IEnumerator AboutUpdate()
    {
        yield return new WaitForSeconds(30);
        
        if(credential != null)
            PrintAbout();
        
        StartCoroutine(AboutUpdate());
    }

    public void SelectFolderInst()
    {
        string result = FolderSelect(_isntFolderInput.text);

        if (result != "-1")
        {
            _folders._folderIDInst = result;
            Save();
        }
        else
        {
            StartCoroutine(InstError());
        }
    }

    IEnumerator InstError()
    {
        _instSelctFolderError.SetActive(true);
        
        yield return new WaitForSeconds(1.5f);
        
        _instSelctFolderError.SetActive(false);
    }
    
    public void SelectFolderMail()
    {
        string result = FolderSelect(_mailFolderInput.text);
        
        if (result != "-1")
        {
            _folders._folderIdMail = result;
            Save();
        }
        else
        {
            StartCoroutine(MailError());
        }
    }
    
    IEnumerator MailError()
    {
        _mailSelctFolderError.SetActive(true);
        
        yield return new WaitForSeconds(1.5f);
        
        _mailSelctFolderError.SetActive(false);
    }
    
    public void SelectFolderSms()
    {
        string result = FolderSelect( _smsFolderInput.text);
        
        if (result != "-1")
        {
            _folders._folderIDSms = result;
            Save();
        }
        else
        {
            StartCoroutine(SmsError());
        }
    }
    
    public void SelectFolderGDrive()
    {
        string result = FolderSelect( _smsFolderInput.text);
        
        if (result != "-1")
        {
            _folders._folderIDSync = result;
            Save();
        }
        else
        {
            StartCoroutine(SyncError());
        }
    }

    IEnumerator SyncError()
    {
        _smsSelctFolderError.SetActive(true);
        
        yield return new WaitForSeconds(1.5f);
        
        _smsSelctFolderError.SetActive(false);
    }
    
    IEnumerator SmsError()
    {
        _smsSelctFolderError.SetActive(true);
        
        yield return new WaitForSeconds(1.5f);
        
        _smsSelctFolderError.SetActive(false);
    }

    private void UiUpdate()
    {
        _isntFolderInput.text = _folders._folderNameIsnt;
        _syncFolderInput.text = _folders._foldernameSync;
        _mailFolderInput.text = _folders._folderNameMail;
        _smsFolderInput.text = _folders._folderNameSms;
    }

    public void CreateInstFolder()
    {
        _folders._folderNameIsnt = _isntFolderInput.text;
        _folders._folderIDInst = CreateFolder(_isntFolderInput.text);
        Save();
    }
    
    public void CreateSyncFolder()
    {
        _folders._foldernameSync = _syncFolderInput.text;
        _folders._folderIDSync = CreateFolder(_syncFolderInput.text,false);
        Save();
    }

    public void CreateFolderMail()
    {
        _folders._folderNameMail = _mailFolderInput.text;
        _folders._folderIdMail = CreateFolderMail(_mailFolderInput.text);
        Save();
    }
    
    public void CreateFolderSms()
    {
        _folders._folderNameSms = _smsFolderInput.text;
        _folders._folderIDSms = CreateFolderMail(_smsFolderInput.text);
        Save();
    }

    IEnumerator ResultFolderCreateHandleInst(bool isGood)
    {
        if (isGood)
        {
            _goodResultInst.SetActive(true);
        }
        else
        {
            _badResultInst.SetActive(true);
        }
        
        yield return new WaitForSeconds(1f);
        
        _goodResultInst.SetActive(false);
        _badResultInst.SetActive(false);
    }
    
    IEnumerator ResultFolderCreateHandleSMS(bool isGood)
    {
        if (isGood)
        {
            _goodResultSms.SetActive(true);
        }
        else
        {
            _badResultSms.SetActive(true);
        }
        
        yield return new WaitForSeconds(1f);
        
        _goodResultSms.SetActive(false);
        _badResultSms.SetActive(false);
    }
    
    
    IEnumerator ResultFolderCreateHandleMail(bool isGood)
    {
        if (isGood)
        {
            _goodResultMail.SetActive(true);
        }
        else
        {
            _badResultMail.SetActive(true);
        }
        
        yield return new WaitForSeconds(1f);
        
        _goodResultMail.SetActive(false);
        _badResultMail.SetActive(false);
    }
    
    IEnumerator ResultFolderCreateHandleSync(bool isGood)
    {
        if (isGood)
        {
            _goodResultSync.SetActive(true);
        }
        else
        {
            _badResultSync.SetActive(true);
        }
        
        yield return new WaitForSeconds(1f);
        
        _goodResultSync.SetActive(false);
        _badResultSync.SetActive(false);
    }
    
    public async void PrintAbout() {
        try {
            string[] scopes = new string[] { DriveService.Scope.Drive,  
                DriveService.Scope.DriveAppdata,   
                DriveService.Scope.DriveFile,   
                DriveService.Scope.DriveMetadataReadonly, 
                DriveService.Scope.DriveReadonly,      
                DriveService.Scope.DriveScripts }; 
    
            string ApplicationName = "Drive API .NET Quickstart";


            if (credential == null)
            {
                using (var stream =
                    new FileStream(_dropBoxManager.GetPath(), FileMode.Open, FileAccess.Read))
                {
                    string credPath = "token.json";
                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        scopes,
                        "user",
                        CancellationToken.None,
                        new FileDataStore(credPath, true)).Result;
                    Debug.Log("Credential file saved to: " + credPath);

                    stream.Close();
                    
                    stream.Dispose();
                }
            }

            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
            
            service.HttpClient.Timeout = TimeSpan.FromMinutes(3);

            var request = service.About.Get();

            request.Fields = "storageQuota";

            var accountInfo = await request.ExecuteAsync();

            float? value = (accountInfo.StorageQuota.Limit - accountInfo.StorageQuota.Usage) / (1024f * 1024f * 1024f);

            if (LocalizationManager._instance.Language == Language.en)
            {
                if (value != null)
                    _freeSpaceText.text = "Free space on drive " + value?.ToString("0.0") + " GB";
            }
            else
            {
                if (value != null)
                    _freeSpaceText.text = "Свободно на ГДРАЙВ " + value?.ToString("0.0")+ " GB";
            }

             //Debug.Log("Storage: " + (accountInfo.StorageQuota.Limit - accountInfo.StorageQuota.Usage) / (1024f * 1024f * 1024f));

        } catch (Exception e) {
            Debug.Log("An error occurred: " + e.Message);
        }
    }


    private void Save()
    {
        PlayerPrefs.SetString("Folders",JsonUtility.ToJson(_folders));
        PlayerPrefs.Save();
    }
    
    public async Task<string> GoogleDriveUpload(string path, bool isInst = true)
    {
       // try
       // {
       Debug.Log(path);
          //  string normalPath = Path.Combine(@Path.GetDirectoryName(path).Replace("\\", "/"), Path.GetFileName(path));
          string normalPath = path;
            
            string[] scopes = new string[] { DriveService.Scope.Drive,  
                DriveService.Scope.DriveAppdata,   
                DriveService.Scope.DriveFile,   
                DriveService.Scope.DriveMetadataReadonly, 
                DriveService.Scope.DriveReadonly,      
                DriveService.Scope.DriveScripts }; 
    
            string ApplicationName = "Drive API .NET Quickstart";


            if (credential == null)
            {
                using (var stream =
                    new FileStream(_dropBoxManager.GetPath(), FileMode.Open, FileAccess.Read))
                {
                    string credPath = "token.json";
                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        scopes,
                        "user",
                        CancellationToken.None,
                        new FileDataStore(credPath, true)).Result;
                    Debug.Log("Credential file saved to: " + credPath);

                    stream.Close();
                    
                    stream.Dispose();
                }
            }
            
           

            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
            
            service.HttpClient.Timeout = TimeSpan.FromMinutes(3);

            string fileName = Path.GetFileName(normalPath);

            string urlByName = GetFileURLByName(fileName, service);

            if (urlByName == "-1")
            {
                string extension = Path.GetExtension(normalPath)?.Replace(".", "");

            string folderID = "-1";
            
            if(isInst)
                folderID = _folders._folderIDInst;
            else 
                folderID = _folders._folderIDSync;
            
            
            var fileMetadata = new File()
            {
                Name =  fileName,
            };

            if (folderID != "-1")
            {
                fileMetadata.Parents = new List<string>() {folderID};
            }

            
            FilesResource.CreateMediaUpload request;

            using (var stream = new System.IO.FileStream(normalPath,
                System.IO.FileMode.Open, FileAccess.Read, FileShare.Read))
            {

                if (extension == "mp4" || extension == "mov" || extension == "MP4" || extension == "MOV" )
                {
                    request = service.Files.Create(
                        fileMetadata, stream, "video/" + extension);
                }
                else
                {
                    request = service.Files.Create(
                        fileMetadata, stream, "image/" + extension);
                }
                
                request.Fields = "id";

                await request.UploadAsync();
                
                stream.Close();
                
                stream.Dispose();
                
            }
            
            Debug.Log(request.ResponseBody);
            
            var file = request.ResponseBody;

            InsertPermission(service, file?.Id, "", "anyone", "reader");
        
            Debug.Log("File ID: " + file.Id);

            string url = GetFileURL(fileName, service, file.Id);

            
            Debug.Log(url);

            return url;
            }

            return urlByName;


       // }
       /* catch (Exception e)
        {
                        
            Debug.Log(e.Message);
            
            _text.text += e.Message + " //// ERROR ";
            
            return "-1";

        }*/
       
    }
    
    
    public async Task<string> GoogleDriveUploadToMail(string path)
         {
             try
             {
                 string normalPath = Path.Combine(@Path.GetDirectoryName(path).Replace("\\", "/"), Path.GetFileName(path));
     
               
                 
                 string[] scopes = new string[] { DriveService.Scope.Drive,  
                     DriveService.Scope.DriveAppdata,   
                     DriveService.Scope.DriveFile,   
                     DriveService.Scope.DriveMetadataReadonly, 
                     DriveService.Scope.DriveReadonly,      
                     DriveService.Scope.DriveScripts }; 
         
                 string ApplicationName = "Drive API .NET Quickstart";
     
     
                 if (credential == null)
                 {
                     using (var stream =
                         new FileStream(_dropBoxManager.GetPath(), FileMode.Open, FileAccess.Read))
                     {
                         string credPath = "token.json";
                         credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                             GoogleClientSecrets.Load(stream).Secrets,
                             scopes,
                             "user",
                             CancellationToken.None,
                             new FileDataStore(credPath, true)).Result;
                         Debug.Log("Credential file saved to: " + credPath);
            
                     
                         stream.Close();
                         
                         stream.Dispose();
                     }
                 }
                 
         
                
     
                 var service = new DriveService(new BaseClientService.Initializer()
                 {
                     HttpClientInitializer = credential,
                     ApplicationName = ApplicationName,
                 });
                 
                 service.HttpClient.Timeout = TimeSpan.FromMinutes(3); 
                 
     
                 
                 string fileName = Path.GetFileName(normalPath);
     
                 string urlByName = GetFileURLByName(fileName, service);
     
                 if (urlByName == "-1")
                 {
                     string extension = Path.GetExtension(normalPath)?.Replace(".", "");
     
                 string folderID = "-1";
     
                 folderID = _folders._folderIdMail;
                 
                 Debug.Log(folderID);
     
     
                 var fileMetadata = new File()
                 {
                     Name =  fileName,
                 };
     
                 if (folderID != "-1")
                 {
                     fileMetadata.Parents = new List<string>() {folderID};
                 }
     
                 
                 FilesResource.CreateMediaUpload request;
                 
          
     
                 using (var stream = new System.IO.FileStream(normalPath,
                     System.IO.FileMode.Open, FileAccess.Read, FileShare.Read))
                 {
     
                     if (extension == "mp4" || extension == "mov" || extension == "MP4" || extension == "MOV" )
                     {
                         request = service.Files.Create(
                             fileMetadata, stream, "video/" + extension);
                     }
                     else
                     {
                         request = service.Files.Create(
                             fileMetadata, stream, "image/" + extension);
                     }
                     
                    
                     
                     request.Fields = "id";
     
                     await request.UploadAsync();
                     
                     stream.Close();
                     
                     stream.Dispose();
                     
                 }
                 
               
                 
                 var file = request.ResponseBody;
     
                 Debug.Log(file);
     
                 InsertPermission(service, file.Id, "", "anyone", "reader");
             
                 Debug.Log("File ID: " + file.Id);
                 
               
                 
                 string url = GetFileURL(fileName, service, file.Id);
           
               
                 
                 Debug.Log(url);
     
                 return url;
                 }
     
                 return urlByName;
     
     
             }
             catch (Exception e)
             {
                             
                 Debug.Log(e.Message);
                 
               
                 
                 return "-1";
     
             }
            
         }

     public async Task<string> GoogleDriveUploadToSms(string path)
         {
             try
             {
                 string normalPath = Path.Combine(@Path.GetDirectoryName(path).Replace("\\", "/"), Path.GetFileName(path));
     
               
                 
                 string[] scopes = new string[] { DriveService.Scope.Drive,  
                     DriveService.Scope.DriveAppdata,   
                     DriveService.Scope.DriveFile,   
                     DriveService.Scope.DriveMetadataReadonly, 
                     DriveService.Scope.DriveReadonly,      
                     DriveService.Scope.DriveScripts }; 
         
                 string ApplicationName = "Drive API .NET Quickstart";
     
     
                 if (credential == null)
                 {
                     using (var stream =
                         new FileStream(_dropBoxManager.GetPath(), FileMode.Open, FileAccess.Read))
                     {
                         string credPath = "token.json";
                         credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                             GoogleClientSecrets.Load(stream).Secrets,
                             scopes,
                             "user",
                             CancellationToken.None,
                             new FileDataStore(credPath, true)).Result;
                         Debug.Log("Credential file saved to: " + credPath);
            
                     
                         stream.Close();
                         
                         stream.Dispose();
                     }
                 }
                 
         
                
     
                 var service = new DriveService(new BaseClientService.Initializer()
                 {
                     HttpClientInitializer = credential,
                     ApplicationName = ApplicationName,
                 });
                 
                 service.HttpClient.Timeout = TimeSpan.FromMinutes(3); 
                 
     
                 
                 string fileName = Path.GetFileName(normalPath);
     
                 string urlByName = GetFileURLByName(fileName, service);
     
                 if (urlByName == "-1")
                 {
                     string extension = Path.GetExtension(normalPath)?.Replace(".", "");
     
                 string folderID = "-1";
     
                 folderID = _folders._folderIDSms;
                 
                 Debug.Log(folderID);
     
     
                 var fileMetadata = new File()
                 {
                     Name =  fileName,
                 };
     
                 if (folderID != "-1")
                 {
                     fileMetadata.Parents = new List<string>() {folderID};
                 }
     
                 
                 FilesResource.CreateMediaUpload request;
                 
          
     
                 using (var stream = new System.IO.FileStream(normalPath,
                     System.IO.FileMode.Open, FileAccess.Read, FileShare.Read))
                 {
     
                     if (extension == "mp4" || extension == "mov" || extension == "MP4" || extension == "MOV" )
                     {
                         request = service.Files.Create(
                             fileMetadata, stream, "video/" + extension);
                     }
                     else
                     {
                         request = service.Files.Create(
                             fileMetadata, stream, "image/" + extension);
                     }
                     
                    
                     
                     request.Fields = "id";
     
                     await request.UploadAsync();
                     
                     stream.Close();
                     
                     stream.Dispose();
                     
                 }
                 
               
                 
                 var file = request.ResponseBody;
     
                 Debug.Log(file);
     
                 InsertPermission(service, file.Id, "", "anyone", "reader");
             
                 Debug.Log("File ID: " + file.Id);
                 
               
                 
                 string url = GetFileURL(fileName, service, file.Id);
           
               
                 
                 Debug.Log(url);
     
                 return url;
                 }
     
                 return urlByName;
     
     
             }
             catch (Exception e)
             {
                             
                 Debug.Log(e.Message);
                 
               
                 
                 return "-1";
     
             }
            
         }
    
    public string CreateFolder(string folderName, bool _isInst = true)
    {
        try
        {
            string[] scopes = new string[]
            {
                DriveService.Scope.Drive,
                DriveService.Scope.DriveAppdata,
                DriveService.Scope.DriveFile,
                DriveService.Scope.DriveMetadataReadonly,
                DriveService.Scope.DriveReadonly,
                DriveService.Scope.DriveScripts
            };

            string ApplicationName = "Drive API .NET Quickstart";


            if (credential == null)
            {
                using (var stream =
                    new FileStream(DropBoxManager._instance.GetPath(), FileMode.Open, FileAccess.Read))
                {
                    string credPath = "token.json";
                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        scopes,
                        "user",
                        CancellationToken.None,
                        new FileDataStore(credPath, true)).Result;
                    Debug.Log("Credential file saved to: " + credPath);
                  

                    stream.Close();

                    stream.Dispose();
                }
            }

            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            var fileMetadata = new Google.Apis.Drive.v3.Data.File()
            {
                Name = folderName,
                MimeType = "application/vnd.google-apps.folder"
            };
            
            Debug.Log(folderName);
            var request = service.Files.Create(fileMetadata);

            request.Fields = "id";

            var file = request.Execute();

            if (_isInst)
                StartCoroutine(ResultFolderCreateHandleInst(true));
            else StartCoroutine(ResultFolderCreateHandleSync(true));
                
            return file.Id;


        }
        catch (Exception e)
        {
            Debug.Log(e);
            if (_isInst)
                StartCoroutine(ResultFolderCreateHandleInst(false));
            else StartCoroutine(ResultFolderCreateHandleSync(false));
            return "-1";
            
        }
    }
    
     public string CreateFolderMail(string folderName)
    {
        try
        {
            string[] scopes = new string[]
            {
                DriveService.Scope.Drive,
                DriveService.Scope.DriveAppdata,
                DriveService.Scope.DriveFile,
                DriveService.Scope.DriveMetadataReadonly,
                DriveService.Scope.DriveReadonly,
                DriveService.Scope.DriveScripts
            };

            string ApplicationName = "Drive API .NET Quickstart";


            if (credential == null)
            {
                using (var stream =
                    new FileStream(DropBoxManager._instance.GetPath(), FileMode.Open, FileAccess.Read))
                {
                    string credPath = "token.json";
                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        scopes,
                        "user",
                        CancellationToken.None,
                        new FileDataStore(credPath, true)).Result;
                    Debug.Log("Credential file saved to: " + credPath);
                  

                    stream.Close();

                    stream.Dispose();
                }
            }

            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            var fileMetadata = new Google.Apis.Drive.v3.Data.File()
            {
                Name = folderName,
                MimeType = "application/vnd.google-apps.folder"
            };
            
            Debug.Log(folderName);
            
            var request = service.Files.Create(fileMetadata);

            request.Fields = "id";

            var file = request.Execute();

            StartCoroutine(ResultFolderCreateHandleMail(true));
                
            return file.Id;


        }
        catch (Exception e)
        {
            Debug.Log(e);
            StartCoroutine(ResultFolderCreateHandleMail(false));
            return "-1";
            
        }
    }

     public string CreateFolderSms(string folderName)
    {
        try
        {
            string[] scopes = new string[]
            {
                DriveService.Scope.Drive,
                DriveService.Scope.DriveAppdata,
                DriveService.Scope.DriveFile,
                DriveService.Scope.DriveMetadataReadonly,
                DriveService.Scope.DriveReadonly,
                DriveService.Scope.DriveScripts
            };

            string ApplicationName = "Drive API .NET Quickstart";


            if (credential == null)
            {
                using (var stream =
                    new FileStream(DropBoxManager._instance.GetPath(), FileMode.Open, FileAccess.Read))
                {
                    string credPath = "token.json";
                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        scopes,
                        "user",
                        CancellationToken.None,
                        new FileDataStore(credPath, true)).Result;
                    Debug.Log("Credential file saved to: " + credPath);
                  

                    stream.Close();

                    stream.Dispose();
                }
            }

            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            var fileMetadata = new Google.Apis.Drive.v3.Data.File()
            {
                Name = folderName,
                MimeType = "application/vnd.google-apps.folder"
            };
            
            Debug.Log(folderName);
            
            var request = service.Files.Create(fileMetadata);

            request.Fields = "id";

            var file = request.Execute();

            StartCoroutine(ResultFolderCreateHandleSMS(true));
                
            return file.Id;


        }
        catch (Exception e)
        {
            Debug.Log(e);
            StartCoroutine(ResultFolderCreateHandleSMS(false));
            return "-1";
            
        }
    }

    public  Permission InsertPermission(DriveService service, String fileId, String value,
        String type, String role) {
        Permission newPermission = new Permission();
        newPermission.Type = type;
        newPermission.Role = role;
        try {
            return service.Permissions.Create(newPermission, fileId).Execute();
        } catch (Exception e) {
            Console.WriteLine("An error occurred: " + e.Message);
           
        }
        return null;
    }
    
    public string GetFileURL(string fileName, DriveService  service, string id)
    {
        try
        {
            FilesResource.ListRequest listRequest = service.Files.List();
            listRequest.PageSize = 10;
            listRequest.Fields = "nextPageToken, files(id, name, parents, properties, shared, webContentLink, webViewLink, fullFileExtension, capabilities)";
            listRequest.Q = string.Format("(name contains '{0}')", fileName);
            IList<Google.Apis.Drive.v3.Data.File> files = listRequest.Execute().Files;
            if (files.Count > 0)
            {
                foreach (var file in files)
                {
                    if (file.Id == id)
                        return file.WebViewLink;
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
           
        }

        return "-1";
    }

    public string FolderSelect(string folderName)
    {
        try
        {
            string[] scopes = new string[]
            {
                DriveService.Scope.Drive,
                DriveService.Scope.DriveAppdata,
                DriveService.Scope.DriveFile,
                DriveService.Scope.DriveMetadataReadonly,
                DriveService.Scope.DriveReadonly,
                DriveService.Scope.DriveScripts
            };

            string ApplicationName = "Drive API .NET Quickstart";


            if (credential == null)
            {
                using (var stream =
                    new FileStream(DropBoxManager._instance.GetPath(), FileMode.Open, FileAccess.Read))
                {
                    string credPath = "token.json";
                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        scopes,
                        "user",
                        CancellationToken.None,
                        new FileDataStore(credPath, true)).Result;
                    Debug.Log("Credential file saved to: " + credPath);
                  

                    stream.Close();

                    stream.Dispose();
                }
            }

            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
            
            FilesResource.ListRequest listRequest = service.Files.List();
            listRequest.PageSize = 10;
            listRequest.Fields = "nextPageToken, files(id, name, parents, properties, shared, webContentLink, webViewLink, fullFileExtension, capabilities)";
            listRequest.Q = string.Format("(name contains '{0}')", folderName);
            IList<Google.Apis.Drive.v3.Data.File> files = listRequest.Execute().Files;
            if (files.Count > 0)
            {
                foreach (var file in files)
                {
                   
                    if (file.Name == folderName)
                        return file.Id;
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
            return "-1";
        }

        return "-1";
    }
    
    public string GetFileURLByName(string fileName, DriveService  service)
    {
        try
        {
            FilesResource.ListRequest listRequest = service.Files.List();
            listRequest.PageSize = 10;
            listRequest.Fields = "nextPageToken, files(id, name, parents, properties, shared, webContentLink, webViewLink, fullFileExtension, capabilities)";
            listRequest.Q = string.Format("(name contains '{0}')", fileName);
            IList<Google.Apis.Drive.v3.Data.File> files = listRequest.Execute().Files;
            if (files.Count > 0)
            {
                foreach (var file in files)
                {
                   
                    if (file.Name == fileName)
                        return file.WebViewLink;
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

        return "-1";
    }

}

[System.Serializable]
public class GoogleDrivesFolderNames
{
    public string _folderIDSync  = "-1";

    public string _folderIDInst  = "-1";
    
    public string _folderIDSms  = "-1";

    public string _folderIdMail = "-1";
    
    public string _folderNameSms  = "";

    public string _folderNameMail = "";

    public string _folderNameIsnt = "";

    public string _foldernameSync = "";
}
