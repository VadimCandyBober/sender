﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendKindsDataBase : MonoBehaviour
{
    public static SendKindsDataBase _instance;

    private SendKindsData _data;

    public SendKindsData Data => _data;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else Destroy(gameObject);
        
        DontDestroyOnLoad(gameObject);

        var dataJson = PlayerPrefs.GetString("SendKinds", "");

        if (dataJson != "")
        {
            _data = JsonUtility.FromJson<SendKindsData>(dataJson);
        }
        else
        {
            _data =  new SendKindsData();
        }
    }


    public void Save()
    {
        PlayerPrefs.SetString("SendKinds", JsonUtility.ToJson(_data));
        PlayerPrefs.Save();
    }

    public bool GetDropBox()
    {
        return _data._dropBox;
    }
    
    
}

[System.Serializable]
public class SendKindsData
{
    public bool _telegram;

    public bool _instagram;

    public bool _mail;

    public bool _dropBox;

    public bool _viber;
}
