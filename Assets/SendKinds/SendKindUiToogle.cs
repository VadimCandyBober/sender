﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SendKindUiToogle : MonoBehaviour
{
   [SerializeField] private Sprite _offToogle;

   [SerializeField] private Sprite _onToogle;
   
   [SerializeField] private Image _instagramToogle;

   [SerializeField] private Image _mailToogle;

   [SerializeField] private Image _dropBoxToogle;
   
   [SerializeField] private Image _telegramToogle;

   private void Start()
   {
      var kindData = FindObjectOfType<SendKindsDataBase>().Data;
      
      SetToogle(_instagramToogle, kindData._instagram);
      SetToogle(_mailToogle, kindData._mail);
      SetToogle(_dropBoxToogle, kindData._dropBox);
      
      SetToogle(_telegramToogle, kindData._telegram);
   }


   public void SetViber()
   {
      SendKindsDataBase._instance.Data._viber = !SendKindsDataBase._instance.Data._viber;
      SendKindsDataBase._instance.Save();
     // SetToogle(_viberToogle,SendKindsDataBase._instance.Data._viber);
   }
   
   public void SetTelegram()
   {
      SendKindsDataBase._instance.Data._telegram = !SendKindsDataBase._instance.Data._telegram;
      SendKindsDataBase._instance.Save();
      SetToogle(_telegramToogle,SendKindsDataBase._instance.Data._telegram);
   }

   public void SetInstagram()
   {
      SendKindsDataBase._instance.Data._instagram = !SendKindsDataBase._instance.Data._instagram;
      SendKindsDataBase._instance.Save();
      SetToogle(_instagramToogle,SendKindsDataBase._instance.Data._instagram);
   }
   
   public void SetMail()
   {
      SendKindsDataBase._instance.Data._mail = !SendKindsDataBase._instance.Data._mail;
      SendKindsDataBase._instance.Save();
      SetToogle(_mailToogle,SendKindsDataBase._instance.Data._mail);
   }
   
   public void SetDropBox()
   {
      SendKindsDataBase._instance.Data._dropBox = !SendKindsDataBase._instance.Data._dropBox;
      SendKindsDataBase._instance.Save();
      //DropBoxManager._instance.UpdateDropBox();
      SetToogle(_dropBoxToogle,SendKindsDataBase._instance.Data._dropBox);
   }
   
   

   private void SetToogle(Image _image, bool _state)
   {
      if (_state)
      {
         _image.sprite = _onToogle;
      }
      else
      {
         _image.sprite = _offToogle;
      }
   }
   
   
}


