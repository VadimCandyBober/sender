﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendKindUI : MonoBehaviour
{
    [SerializeField] private GameObject _dropBox;

    [SerializeField] private GameObject _instagram;

    [SerializeField] private GameObject _viber;

    [SerializeField] private GameObject _mail;

    [SerializeField] private GameObject _telegram;
    
    [SerializeField] private GameObject _sms;

    private void OffAll()
    {
        _dropBox.SetActive(false);
        _instagram.SetActive(false);
        _viber.SetActive(false);
        _mail.SetActive(false);
        _telegram.SetActive(false);
        _sms.SetActive(false);
    }

    public void OnDropBox()
    {
        OffAll();
        _dropBox.SetActive(true);
    }
    
    public void OnSms()
    {
        OffAll();
        _sms.SetActive(true);
    }
    
    public void OnMail()
    {
        OffAll();
        _mail.SetActive(true);
    }
    
    public void OnViber()
    {
        OffAll();
        _viber.SetActive(true);
    }
    
    public void OnTelegram()
    {
        OffAll();
        _telegram.SetActive(true);
    }
    
    public void OnInstagram()
    {
        OffAll();
        _instagram.SetActive(true);
    }
}
