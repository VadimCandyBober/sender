﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class DesignSetUp : MonoBehaviour
{
    private DesignData _designData;

    [SerializeField] private Image _backGround;

    [SerializeField] private Image _preview;

    private void OnEnable()
    {
        var json = PlayerPrefs.GetString("DesignData", "");

        if (json != "")
        {
            _designData = JsonUtility.FromJson<DesignData>(json);

            if (_designData._backgroundPath != "")
            {
                Texture2D texture = new Texture2D(1280, 720);
                var fileContent = File.ReadAllBytes(_designData._backgroundPath);
                texture.LoadImage(fileContent);
                _backGround.sprite = texture.ConvertToSprite();
            }
        }
        else
        {
            _designData = new DesignData();
        }
    }
    
    
}
