﻿using System.Collections;
using System.Collections.Generic;
using Crosstales.FB;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class DesignDataManager : MonoBehaviour
{
   public static DesignDataManager _instance;

   private DesignData _designData;

   [SerializeField] private Text _previewText;

   [SerializeField] private Text _backGroundText;

   [SerializeField] private GameObject _blackActive;

   [SerializeField] private GameObject _whiteActive;

   private void Awake()
   {
      _instance = this;
   }

   private void Start()
   {
      var json = PlayerPrefs.GetString("DesignData", "");

      if (json != "")
      {
         _designData = JsonUtility.FromJson<DesignData>(json);
      }
      else
      {
         _designData = new DesignData();
      }
      
      UIUpdate();
   }


   private void UIUpdate()
   {
      _previewText.text = _designData._previewPath;

      _backGroundText.text = _designData._backgroundPath;

      if (_designData._keyboard == "white")
      {
         _whiteActive.SetActive(true);
         _blackActive.SetActive(false);
      }
      else
      {
         _whiteActive.SetActive(false);
         _blackActive.SetActive(true);
      }
   }

   private void Save()
   {
      PlayerPrefs.SetString("DesignData", JsonUtility.ToJson(_designData));
      PlayerPrefs.Save();
   }

   public void KeyBoardSet(string keyboard)
   {
      _designData._keyboard = keyboard;
      UIUpdate();
      Save();
   }

   public void SetBackGroundPath()
   {
      _designData._backgroundPath = FileBrowser.OpenSingleFile("png");
      UIUpdate();
      Save();
   }

   public void SetPreviewPath()
   {
      _designData._previewPath = FileBrowser.OpenSingleFile("png");
      UIUpdate();
      Save();
   }
   
   
}


[System.Serializable]
public class DesignData
{
   public string _backgroundPath;

   public string _previewPath;

   public string _keyboard = "white";
}
