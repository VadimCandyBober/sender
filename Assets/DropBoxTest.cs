﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;
using Dropbox.Api;
using Dropbox.Api.Files;
using UnityEditor;


public class DropBoxTest : MonoBehaviour
{
    private static readonly HttpClient client = new HttpClient();
    
    private string accesToken;

    private void Start()
    {
        FileUploadToDropbox("", "test.jpg", Application.dataPath + "/Resources/test.jpg");
    }
    
    private static async Task FileUploadToDropbox(string filePath, string fileName, string fileSource)
    {
        using (var dbx = new DropboxClient("H7zJqzs0PYAAAAAAAAAAN1RypBRE1aTAisafQMM2zD0UtlPxOxZyfs1nFarFq5oj"))
        using (var fs = new FileStream(fileSource, FileMode.Open, FileAccess.Read))
        {
            var updated = await dbx.Files.UploadAsync(
                (filePath + "/" + fileName), WriteMode.Overwrite.Instance, body: fs);
            
            Debug.Log("rev {2} " + updated.Rev);
         
        }
        
        Debug.Log("test");
    }

}
