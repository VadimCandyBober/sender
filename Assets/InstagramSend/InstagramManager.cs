﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;
using InstagramApiSharp;
using InstagramApiSharp.API.Builder;
using InstagramApiSharp.Classes;
using InstagramApiSharp.Classes.Android.DeviceInfo;
using InstagramApiSharp.Classes.Models;
using InstagramApiSharp.Classes.SessionHandlers;
using InstagramApiSharp.Enums;
using UnityEngine.UI;

public class InstagramManager : MonoBehaviour
{
    private InstagramApiSharp.API.IInstaApi _instaApi;

    [SerializeField] private InputField _login;

    [SerializeField] private InputField _password;

    [SerializeField] private InputField _mailBody;
    
    private InstagramInfo _instagramInfo;

    public static InstagramManager _instance;

    private AndroidDevice _device;

    [SerializeField] private GameObject _loginButton;

    [SerializeField] private GameObject _loginProcessing;

    [SerializeField] private GameObject _loginError;

    [SerializeField] private GameObject _loginSucces;

    [SerializeField] private GameObject _inputCodeBox;
    
    [SerializeField] private GameObject _sendCodeBox;
    
    [SerializeField] private InputField _codeField;

    [SerializeField] private GameObject _loginUI;

    [SerializeField] private Text _loginText;
    
    [SerializeField] private GameObject _spawnWindow;

    
    private UnityMainThreadDispatcher _Dispatcher;

    private GoogleDriveTest _googleDrive;

    public GameObject _errorBox;

    public Text _errorText;

    private void Awake()
    {
        _googleDrive = FindObjectOfType<GoogleDriveTest>();
        _Dispatcher = FindObjectOfType<UnityMainThreadDispatcher>();
    }
    private void Start()
    {
        
        if (_instance == null)
        {
            _instance = this;
        } else Destroy(gameObject);
        
        var json = PlayerPrefs.GetString("InstaData","");

        if (json != "")
        {
            _instagramInfo = JsonUtility.FromJson<InstagramInfo>(json);
            UiSetUp();
        } else 
            _instagramInfo = new InstagramInfo();
        
        UiSetUp();
        Login(false);

    }

    private IEnumerator LoginHandle(bool succes)
    {
        yield return new WaitForSeconds(1f);

        var lang =  LocalizationManager._instance.Language;

        if (lang == Language.en)
        {
            if (succes)
            {
                _loginText.text = "INSTAGRAM LOGIN SUCCES";
            }
            else
            {
                _loginText.text = "INSTAGRAM LOGIN ERROR";
            }
        }
        else
        {
            if (succes)
            {
                _loginText.text = "ВХОД В INSTAGRAM УСПЕШЕН";
            }
            else
            {
                _loginText.text = "ОШИБКА ВХОДА В INSTAGRAM";
            }
        }
        
        _loginUI.SetActive(true);

        yield return new WaitForSeconds(1.5f);
        
        _loginUI.SetActive(false);
    }

    public void LoginProcessingButton()
    {
        _inputCodeBox.SetActive(false);
        _sendCodeBox.SetActive(false);
        _loginButton.SetActive(false);
        _loginSucces.SetActive(false);
        _loginError.SetActive(false);
        _loginProcessing.SetActive(true);
    }
    
    public void LoginButton()
    {
        _inputCodeBox.SetActive(false);
        _sendCodeBox.SetActive(false);
        _loginButton.SetActive(true);
        _loginSucces.SetActive(false);
        _loginError.SetActive(false);
        _loginProcessing.SetActive(false);
    }
    
    public void LoginError()
    {
        _inputCodeBox.SetActive(false);
        _sendCodeBox.SetActive(false);
        _loginButton.SetActive(false);
        _loginSucces.SetActive(false);
        _loginError.SetActive(true);
        _loginProcessing.SetActive(false);
    }
    
    public void LoginSucces()
    {
        _inputCodeBox.SetActive(false);
        _sendCodeBox.SetActive(false);
        _loginButton.SetActive(false);
        _loginSucces.SetActive(true);
        _loginError.SetActive(false);
        _loginProcessing.SetActive(false);
    }

    private void UiSetUp()
    {
        _login.text = _instagramInfo._login;
        _password.text = _instagramInfo._pass;
        _mailBody.text = _instagramInfo._mailBody;
    }

    public void SetLogin()
    {
        _instagramInfo._login = _login.text;
        Save();
    }

    public void SetPassword()
    {
        _instagramInfo._pass = _password.text;
        Save();
    }

    public void SetBody()
    {
        _instagramInfo._mailBody = _mailBody.text;
        Save();
    }


    private void Save()
    {
        PlayerPrefs.SetString("InstaData", JsonUtility.ToJson(_instagramInfo));
        PlayerPrefs.Save();
    }

    private void Update()
    {

        if(Input.GetKeyDown(KeyCode.T) && _instaApi != null)
            Debug.Log(_instaApi.IsUserAuthenticated);

       
    }


    public void NewLogin()
    {
       Login();
    }
    
    public async Task<string> Relogin()
    {
        string state = "-1";
        
        try
        {
            File.Delete(_instagramInfo._login + "_state.bin");
        
            string StateFile = _instagramInfo._login + "_state.bin";

            var userSession = new UserSessionData
            {
                UserName = _instagramInfo._login,
                Password = _instagramInfo._pass
            };

            _instaApi = InstaApiBuilder.CreateBuilder()
                .SetUser(userSession)
                .SetRequestDelay(RequestDelay.FromSeconds(0, 1))
                .SetSessionHandler(new FileSessionHandler() {FilePath =  StateFile })
                .Build();
            if (!_instaApi.IsUserAuthenticated)
            {
                var logInResult = await _instaApi.LoginAsync();
                
                if (logInResult.Succeeded)
                {
                    LoginSucces();
                    
                    SaveSession();
                    
                    state = "Good Login";
                }
                else 
                {
                    if (logInResult.Value == InstaLoginResult.BadPassword)
                    {
                        StartCoroutine(LoginErrorHandle());
                    }
                    if (logInResult.Value == InstaLoginResult.ChallengeRequired)
                    {
                        var challenge = await _instaApi.GetChallengeRequireVerifyMethodAsync();
                        if (challenge.Succeeded)
                        {
                            if (challenge.Value.SubmitPhoneRequired)
                            {
                               
                            }
                            else
                            {
                                if (challenge.Value.StepData != null)
                                {
                                    if (!string.IsNullOrEmpty(challenge.Value.StepData.PhoneNumber))
                                    {
                                        Debug.Log("Verifi phone" + challenge.Value.StepData.PhoneNumber);
                                    }
                                    if (!string.IsNullOrEmpty(challenge.Value.StepData.Email))
                                    {
                                        Debug.Log("Verifi phone" + challenge.Value.StepData.PhoneNumber);
                                    }
                                    state = "Phone";

                                    Debug.Log("Chalenge size");
                                }
                            }
                            
                            SendCodeBox();
                        }
                        else
                        {
                            state = "Error";
                        }
                         
                    }
                    else if(logInResult.Value == InstaLoginResult.TwoFactorRequired)
                    {
                        state = "Error";
                    }
                }
            }
            else
            {
                state = "Good Login";
                LoginSucces();
            }
            
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

        return state;
    }
    

    public void DeleteSession()
    {
        try
        {
            File.Delete(_instagramInfo._login + "_state.bin");
        
            Login();
        }
        catch (Exception e)
        {
           Debug.Log(e);
        }
      
    }
    
    

    public async void Login(bool fullLogin = true)
    {
        try
        {
            string StateFile = _instagramInfo._login + "_state.bin";
            
            LoginProcessingButton();
            
            var userSession = new UserSessionData
            {
                UserName = _instagramInfo._login,
                Password = _instagramInfo._pass
            };

            _instaApi = InstaApiBuilder.CreateBuilder()
                .SetUser(userSession)
                .SetRequestDelay(RequestDelay.FromSeconds(0, 1))
                .SetSessionHandler(new FileSessionHandler() {FilePath =  StateFile })
                .Build();
            Debug.Log("Connecting");
            LoadSession();
            if (fullLogin)
            {
                 if (!_instaApi.IsUserAuthenticated)
                {
                var logInResult = await _instaApi.LoginAsync();
            Debug.Log(logInResult.Value);
                if (logInResult.Succeeded)
                {
                    LoginSucces();
                    SaveSession();
                }
                else 
                {
                    if (logInResult.Value == InstaLoginResult.BadPassword)
                    {
                        StartCoroutine(LoginErrorHandle());
                    }
                    if (logInResult.Value == InstaLoginResult.ChallengeRequired)
                    {
                        var challenge = await _instaApi.GetChallengeRequireVerifyMethodAsync();
                        if (challenge.Succeeded)
                        {
                            if (challenge.Value.SubmitPhoneRequired)
                            {
                                Debug.Log("SubmitPhoneChallengeGroup");
                            }
                            else
                            {
                                if (challenge.Value.StepData != null)
                                {
                                    if (!string.IsNullOrEmpty(challenge.Value.StepData.PhoneNumber))
                                    {
                                        Debug.Log("Verifi phone" + challenge.Value.StepData.PhoneNumber);
                                    }
                                    if (!string.IsNullOrEmpty(challenge.Value.StepData.Email))
                                    {
                                        Debug.Log("Verifi phone" + challenge.Value.StepData.PhoneNumber);
                                    }

                                    Debug.Log("Chalenge size");
                                }
                            }
                            
                            SendCodeBox();
                        }
                        else
                        {
                            StartCoroutine(LoginErrorHandle());
                        }
                         
                    }
                    else if(logInResult.Value == InstaLoginResult.TwoFactorRequired)
                    {
                        Debug.Log("Two factor");
                        StartCoroutine(LoginErrorHandle());
                    }
                }
            }
            else
            {
                Debug.Log("Connected"); 
                LoginSucces();
            }
            }
            else
            {
                if (_instaApi.IsUserAuthenticated)
                {
                    Debug.Log("Connected");
                    StartCoroutine(LoginHandle(true));
                    LoginSucces();
                }
                else
                {
                    StartCoroutine(LoginHandle(false));
                    StartCoroutine(LoginErrorHandle());
                }
            }
                
        }
        catch (Exception e)
        {
           Debug.Log("No Login" + e.Data);
        }
       
               
    }

    private void SendCodeBox()
    {
        _inputCodeBox.SetActive(false);
        _sendCodeBox.SetActive(true);
        _loginButton.SetActive(false);
        _loginSucces.SetActive(false);
        _loginError.SetActive(false);
        _loginProcessing.SetActive(false);
    }

    private async void InputCodeBox()
    {
        _inputCodeBox.SetActive(true);
        _sendCodeBox.SetActive(false);
        _loginButton.SetActive(false);
        _loginSucces.SetActive(false);
        _loginError.SetActive(false);
        _loginProcessing.SetActive(false);
    }

    public async void SendEmail()
    {
        try
        {
            var email = await _instaApi.RequestVerifyCodeToEmailForChallengeRequireAsync();
            
            if (email.Succeeded)
            {
                InputCodeBox();
            }
            else
                StartCoroutine(LoginErrorHandle());
        }
        catch (Exception e)
        {
            StartCoroutine(LoginErrorHandle());
        }
    }

    public async void SendSms()
    {
        try
        {
            var phoneNumber = await _instaApi.RequestVerifyCodeToSMSForChallengeRequireAsync();

            if (phoneNumber.Succeeded)
            {
                InputCodeBox();
            }
            else
                StartCoroutine(LoginErrorHandle());
        }
        catch
        {
            StartCoroutine(LoginErrorHandle());
        }
    }

    public async void SendCode()
    {
        LoginProcessingButton();

       string code = _codeField.text.Replace(" ", "");

       var verifyLogin = await _instaApi.VerifyCodeForChallengeRequireAsync(code);
        
        if (verifyLogin.Succeeded)
        {
            Debug.Log("SUCCES");
            SaveSession();
            LoginSucces();
            Debug.Log("CONNECTED");
        }
        else
        {
            Debug.Log("ERROS");
            StartCoroutine(LoginErrorHandle());
            if (verifyLogin.Value == InstaLoginResult.TwoFactorRequired)
            {
                Debug.Log("TWO FACTOR");
            }
        }

        _codeField.text = "";
    }
    
    public async Task<string> ResendCode(string code)
    {
        string state = "-1";

        code = code.Replace(" ", "");

        var verifyLogin = await _instaApi.VerifyCodeForChallengeRequireAsync(code);
        
        if (verifyLogin.Succeeded)
        {
            SaveSession();
            LoginSucces();
            state = "succes";
        }
        else
        {
            state = "error";
        }

        return state;
    }
    
  
    void LoadSession()
    {
        _instaApi?.SessionHandler?.Load();
    }
    void SaveSession()
    {
        if (_instaApi == null)
            return;
        if (!_instaApi.IsUserAuthenticated)
            return;
        _instaApi.SessionHandler.Save();
    }

    
    

    IEnumerator LoginErrorHandle()
    {
        LoginError();
        
        yield return new WaitForSeconds(1.5f);
        
        LoginButton();
        
    }
   
    
   
    public async Task SendDirectMessage(string to, string path)
    {
        try
        {
            string fileName = "-1";

            string time = "-1";
        
            var desireUsername = to;
        
            fileName = Path.GetFileName(path);

            time = DateTime.Now.ToString();
        
            SendedList._instance.AddToItemList(
                new SendedItem("Instagram",to, "NO", fileName, time, path));
        
            var user = await _instaApi.UserProcessor.GetUserAsync(desireUsername);
        
            var userId = user.Value.Pk.ToString();
        
            var directText = await _instaApi.MessagingProcessor
                .SendDirectTextAsync(userId, null, _instagramInfo._mailBody);

            var video = new InstaVideo
            {
                Uri = path
            };

            var videoToUpload = new InstaVideoUpload
            {
                Video = video
            };
        
            Debug.Log(video.Uri);
       
            var directImage = await _instaApi.MessagingProcessor.SendDirectVideoToRecipientsAsync(videoToUpload, userId);
        
            if (directImage.Succeeded)
            {
                SendedList._instance.UpdateStat("Instagram", to, fileName,time);
            }
            else
            {
                Debug.Log(directImage.Info);
                Debug.Log(directImage.Info.Exception);
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
       
        
    }
    
    
    public async Task SendDirectImage(string to, string path)
    {
        try
        {
            if (_instaApi.IsUserAuthenticated)
            {
                Debug.Log("PATH" + path);
                
                if (path.StartsWith("file") || path.StartsWith("/"))
                {
                    path = path.Replace("file:///", "");
                    path = Regex.Replace(path, "^/", "");
                }

                string fileName = "-1";

                string time = "-1";

                var desireUsername = to;

                fileName = Path.GetFileName(path);

                time = DateTime.Now.ToString();

                _Dispatcher.Enqueue(() =>
                {
                    SendedList._instance.AddToItemList(
                        new SendedItem("Instagram", to, "NO", fileName, time, path));
                });

                string url = await _googleDrive.GoogleDriveUpload(path);

                var user = await _instaApi.UserProcessor.GetUserAsync(desireUsername);

                var userId = user.Value.Pk.ToString();

                var text = _instagramInfo._mailBody;
                ;

                if (text == "")
                    text = "!";

                fileName = Path.GetFileName(path);

                if (url != "-1")
                {
                    var directLink =
                        await _instaApi.MessagingProcessor.SendDirectLinkToRecipientsAsync(text, url, userId);

                    if (directLink.Succeeded)
                    {
                        _Dispatcher.Enqueue(() => { SendedList._instance.UpdateStat("Instagram", to, fileName, time); });
                    }
                    else
                    {
                        Debug.Log(directLink.Info.ToString());
                        
                        Debug.Log(directLink.Info.Exception);
                        
                        if (directLink.Info.ToString() == "Spam: feedback_required." || directLink.Info.Exception .ToString()== "Spam: feedback_required.")
                        {
                            OpenSpamWindow();
                        }

                        await ReSendDirectImage(to, path, time, fileName, 5);
                    }
                }
            }
            else
            {
                StartCoroutine(ErrorHandle());
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);

            if (e.ToString() == "Spam: feedback_required.")
            {
                OpenSpamWindow();
            }
        }
    }

    public void OpenSpamWindow()
    {
        _spawnWindow.SetActive(true);
    }

    public void CloseSpamWindow()
    {
        _spawnWindow.SetActive(false);
    }

    IEnumerator ErrorHandle()
    {
        _errorBox.SetActive(true);

        if (LocalizationManager._instance.Language == Language.en)
            _errorText.text = "NO LOGIN IN INST";
        else  _errorText.text = "Не вошли в инстаграм";
        
        yield return new WaitForSeconds(2);
        
        _errorBox.SetActive(false);
    }

    public async Task ReSendDirectImage(string to, string path, string time, string fileName, int delay = 0)
    {
        try
        {
            if (_instaApi.IsUserAuthenticated)
            {
                if (path.StartsWith("file"))
                {
                    path = path.Replace("file:///", "");
                     Debug.Log(path);
                }
                
                await Task.Delay(delay * 1000);
            
                var desireUsername = to;
        
                var user = await _instaApi.UserProcessor.GetUserAsync(desireUsername);
        
                var userId = user.Value.Pk.ToString();

                string url = await _googleDrive.GoogleDriveUpload(path);
        
                var text =  _instagramInfo._mailBody;;

                if (text == "")
                    text = "!";

                if (url != "-1")
                { 
                    var directLink = await _instaApi.MessagingProcessor.SendDirectLinkToRecipientsAsync(text, url, userId);

                    if (directLink.Succeeded)
                    {
                        _Dispatcher.Enqueue(() =>
                        {
                            SendedList._instance.UpdateStat("Instagram", to, fileName, time);
                        });
                    }
                    else
                    {
                        
                        if (directLink.Info.ToString() == "Spam: feedback_required." || directLink.Info.Exception .ToString()== "Spam: feedback_required.")
                        {
                            OpenSpamWindow();
                        }
                        
             
                        Debug.Log(directLink.Info.ToString());
                    
                        Debug.Log(directLink.Info.Exception);
                    }
                
                }
            }
            else
            {
                StartCoroutine(ErrorHandle());
            }
        }
        catch (Exception e)
        {
            if (e.ToString() == "Spam: feedback_required.")
            {
                OpenSpamWindow();
            }
            
            _Dispatcher.Enqueue(() =>
            {
                Debug.Log(e);

                var items = FindObjectsOfType<SendedListItemUI>();

                for (int i = 0; i < items.Length; i++)
                {
                    if (items[i].SendedItem._path == path && items[i].SendedItem._login == to &&
                        items[i].SendedItem._time == time)
                        items[i].OffAnim();
                }
            });
        }
        
       
    }
    
    public async Task ResendSendDirectMessage(string to, string path, string time, string fileName)
    {
        try
        {
            if (path.StartsWith("file"))
            {
                path = path.Replace("file:///", "");
                // Debug.Log(path);
            }

            
            var desireUsername = to;
        
            var user = await _instaApi.UserProcessor.GetUserAsync(desireUsername);
        
            var userId = user.Value.Pk.ToString();
        
            var directText = await _instaApi.MessagingProcessor
                .SendDirectTextAsync(userId, null, _instagramInfo._mailBody);

            var video = new InstaVideo
            {
                // some video path
                Uri = path
            };

            var videoToUpload = new InstaVideoUpload
            {
                Video = video
            };
                      
            Debug.Log(video.Uri);
       
            var directImage = await _instaApi.MessagingProcessor.SendDirectVideoToRecipientsAsync(videoToUpload, userId);
        
            if (directImage.Succeeded)
            {
                Debug.Log("LOL");
                SendedList._instance.UpdateStat("Instagram", to, fileName,time);
            }
            else
            {
                Debug.Log(directImage.Info);
                Debug.Log(directImage.Info.Exception);
                return;
            }


        }
        catch (Exception e)
        {
           Debug.Log(e);
        }
        
        
        
    }
    
}

[System.Serializable]
public class InstagramInfo
{
    public string _login = "";

    public string _pass = "";

    public string _mailBody;

}
