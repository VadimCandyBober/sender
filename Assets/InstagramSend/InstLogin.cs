﻿using System;
using System.Collections;
using System.Collections.Generic;
using InstagramApiSharp.API;
using InstagramApiSharp.API.Builder;
using InstagramApiSharp.Classes;
using InstagramApiSharp.Classes.SessionHandlers;
using UnityEngine;
using UnityEngine.UI;

public class InstLogin : MonoBehaviour
{
    
        const string AppName = "Challenge Required";
        
        const string StateFile = "state.bin";
        
        private static IInstaApi InstaApi;

        public InputField _filed;

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.T))
                LoginButton_Click();
            if(Input.GetKeyDown(KeyCode.A))
                Debug.Log(InstaApi.IsUserAuthenticated);
            
            if(Input.GetKeyDown(KeyCode.Space))
                SubmitPhoneChallengeButton_Click();
            
            if(Input.GetKeyDown(KeyCode.P))
                VerifyButton_Click();
            
            if(Input.GetKeyDown(KeyCode.O))
                SendCodeButton_Click();
        }


        private async void LoginButton_Click()
        {
            Debug.Log("Normal");
            
            var userSession = new UserSessionData
            {
                UserName = "mr.vadimul@gmail.com",
                Password = "3edccde3"
            };

            InstaApi = InstaApiBuilder.CreateBuilder()
                .SetUser(userSession)
                .SetRequestDelay(RequestDelay.FromSeconds(0, 1))
                // Session handler, set a file path to save/load your state/session data
                .SetSessionHandler(new FileSessionHandler() {FilePath =  StateFile })
                .Build();
            Debug.Log("Connecting");
            //Load session
            LoadSession();
            
            if (!InstaApi.IsUserAuthenticated)
            {
                var logInResult = await InstaApi.LoginAsync();
                Debug.Log(logInResult.Value);
                if (logInResult.Succeeded)
                {
                    Debug.Log("Succes");
                    // Save session 
                    SaveSession();
                }
                else
                {
                    if (logInResult.Value == InstaLoginResult.ChallengeRequired)
                    {
                        var challenge = await InstaApi.GetChallengeRequireVerifyMethodAsync();
                        if (challenge.Succeeded)
                        {
                            if (challenge.Value.SubmitPhoneRequired)
                            {
                                Debug.Log("SubmitPhoneChallengeGroup");
                            }
                            else
                            {
                                if (challenge.Value.StepData != null)
                                {
                                    if (!string.IsNullOrEmpty(challenge.Value.StepData.PhoneNumber))
                                    {
                                        Debug.Log("Verifi phone");
                                    }
                                    if (!string.IsNullOrEmpty(challenge.Value.StepData.Email))
                                    {
                                        Debug.Log("Verifi mail");
                                    }

                                    Debug.Log("Chalenge size");
                                }
                            }
                        }
                        else
                            Debug.Log(challenge.Info.Message + " MESSAGE BOX");
                    }
                    else if(logInResult.Value == InstaLoginResult.TwoFactorRequired)
                    {
                        Debug.Log("Two factor");
                    }
                }
            }
            else
            {
                Debug.Log("Connected");
            }
        }

        private async void SubmitPhoneChallengeButton_Click()
        {
            try
            {
                if (string.IsNullOrEmpty("+375333795416") ||
                    string.IsNullOrWhiteSpace("+375333795416"))
                {
                    Debug.Log("Please type a valid phone number(with country code).\r\ni.e: +989123456789");
                    return;
                }

                var phoneNumber = "+375333795416";
                if (!phoneNumber.StartsWith("+"))
                    phoneNumber = $"+{phoneNumber}";

                var submitPhone = await InstaApi.SubmitPhoneNumberForChallengeRequireAsync(phoneNumber);
                
                if (submitPhone.Succeeded)
                {
                    Debug.Log("SUBMITED");
                }
                else
                    Debug.Log(submitPhone.Info.Message + " ERR");

            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message + " EX");
            }
        }
        private async void SendCodeButton_Click()
        {

                bool isEmail = false;
            //if (RadioVerifyWithPhoneNumber.Checked)
            //    isEmail = false;

            try
            {
                // Note: every request to this endpoint is limited to 60 seconds                 
                if (isEmail)
                {
                    // send verification code to email
                    var email = await InstaApi.RequestVerifyCodeToEmailForChallengeRequireAsync();
                    if (email.Succeeded)
                    {
                      Debug.Log("We sent verify code to this email:\n{email.Value.StepData.ContactPoint}");

                      
                    }
                    else
                        Debug.Log("ERROR");
                }
                else
                {
                    // send verification code to phone number
                    var phoneNumber = await InstaApi.RequestVerifyCodeToSMSForChallengeRequireAsync();
                    if (phoneNumber.Succeeded)
                    {
                        Debug.Log("SMS");
                    }
                    else
                        Debug.Log("ERROS");
                }
            }
            catch (Exception ex) {  Debug.Log(ex.Message + "EX"); }

        }

       /* private async void ResendButton_Click(object sender, EventArgs e)
        {
            bool isEmail = false;
            if (RadioVerifyWithEmail.Checked)
                isEmail = true;

            try
            {
                // Note: every request to this endpoint is limited to 60 seconds                 
                if (isEmail)
                {
                    // send verification code to email
                    var email = await InstaApi.RequestVerifyCodeToEmailForChallengeRequireAsync(replayChallenge: true);
                    if (email.Succeeded)
                    {
                        LblForSmsEmail.Text = $"We sent verification code one more time\r\nto this email:\n{email.Value.StepData.ContactPoint}";
                        VerifyCodeGroupBox.Visible = true;
                        SelectMethodGroupBox.Visible = false;
                    }
                    else
                        MessageBox.Show(email.Info.Message, "ERR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    // send verification code to phone number
                    var phoneNumber = await InstaApi.RequestVerifyCodeToSMSForChallengeRequireAsync(replayChallenge: true);
                    if (phoneNumber.Succeeded)
                    {
                        LblForSmsEmail.Text = $"We sent verification code one more time\r\nto this phone number(it's end with this):{phoneNumber.Value.StepData.ContactPoint}";
                        VerifyCodeGroupBox.Visible = true;
                        SelectMethodGroupBox.Visible = false;
                    }
                    else
                        MessageBox.Show(phoneNumber.Info.Message, "ERR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "EX", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }*/

        private async void VerifyButton_Click()
        {

            {
                // Note: calling VerifyCodeForChallengeRequireAsync function, 
                // if user has two factor enabled, will wait 15 seconds and it will try to
                // call LoginAsync.

                var verifyLogin = await InstaApi.VerifyCodeForChallengeRequireAsync(_filed.text);
                if (verifyLogin.Succeeded)
                {
                    // you are logged in sucessfully.
                    Debug.Log("SUCCES");
                    // Save session
                    SaveSession();
                    Debug.Log("CONNECTED");
                }
                else
                {
                    Debug.Log("ERROS");
                    // two factor is required
                    if (verifyLogin.Value == InstaLoginResult.TwoFactorRequired)
                    {
                        Debug.Log("TWO FACTOR");
                    }
                   
                }

            }

        }
     /*   private async void TwoFactorButton_Click(object sender, EventArgs e)
        {
            if (InstaApi == null)
                return;
            if (string.IsNullOrEmpty(txtTwoFactorCode.Text))
            {
                MessageBox.Show("Please type your two factor code and then press Auth button.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            // send two factor code
            var twoFactorLogin = await InstaApi.TwoFactorLoginAsync(txtTwoFactorCode.Text);
            Debug.WriteLine(twoFactorLogin.Value);
            if (twoFactorLogin.Succeeded)
            {
                // connected
                // save session
                SaveSession();
                Size = ChallengeSize;
                TwoFactorGroupBox.Visible = false;
                GetFeedButton.Visible = true;
                Text = $"{AppName} Connected";
                Size = NormalSize;
            }
            else
            {
                MessageBox.Show(twoFactorLogin.Info.Message, "ERR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private async void GetFeedButton_Click(object sender, EventArgs e)
        {
            if (InstaApi == null)
            {
                MessageBox.Show("Login first.");
                return;
            }
            if (!InstaApi.IsUserAuthenticated)
            {
                MessageBox.Show("Login first.");
                return;
            }

            var topicalExplore = await InstaApi.FeedProcessor.GetTopicalExploreFeedAsync(PaginationParameters.MaxPagesToLoad(1));

            if (topicalExplore.Succeeded == false)
            {
                if (topicalExplore.Info.ResponseType == ResponseType.ChallengeRequired)
                {
                    var challengeData = await InstaApi.GetLoggedInChallengeDataInfoAsync();
                    // Do something to challenge data, if you want!

                    var acceptChallenge = await InstaApi.AcceptChallengeAsync();
                    // If Succeeded was TRUE, you can continue to your work!
                }
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                StringBuilder sb2 = new StringBuilder();
                sb2.AppendLine("Like 5 Media>");
                foreach (var item in topicalExplore.Value.Medias.Take(5))
                {
                    // like media...
                    var liked = await InstaApi.MediaProcessor.LikeMediaAsync(item.InstaIdentifier);
                    sb2.AppendLine($"{item.InstaIdentifier} liked? {liked.Succeeded}");
                }

                sb.AppendLine("Explore categories: " + topicalExplore.Value.Clusters.Count);
                int ix = 1;
                foreach (var cluster in topicalExplore.Value.Clusters)
                    sb.AppendLine($"#{ix++} {cluster.Name}");

                sb.AppendLine();
                sb.AppendLine();
                sb.AppendLine("Explore tv channels: " + topicalExplore.Value.TVChannels.Count);
                sb.AppendLine();
                sb.AppendLine();

                sb.AppendLine("Explore Feeds Result: " + topicalExplore.Succeeded);
                foreach (var media in topicalExplore.Value.Medias)
                    sb.AppendLine(DebugUtils.PrintMedia("Feed media", media));

                RtBox.Text = sb2.ToString() + Environment.NewLine + Environment.NewLine + Environment.NewLine;

                RtBox.Text += sb.ToString();
                RtBox.Visible = true;
                Size = ChallengeSize;
            }


            //// old explore page
            //var x = await InstaApi.FeedProcessor.GetExploreFeedAsync(PaginationParameters.MaxPagesToLoad(1));
            //if (x.Succeeded == false)
            //{
            //    if (x.Info.ResponseType == ResponseType.ChallengeRequired)
            //    {
            //        var challengeData = await InstaApi.GetLoggedInChallengeDataInfoAsync();
            //        // Do something to challenge data, if you want!

            //        var acceptChallenge = await InstaApi.AcceptChallengeAsync();
            //        // If Succeeded was TRUE, you can continue to your work!
            //    }
            //}
            //else
            //{
            //    StringBuilder sb = new StringBuilder();
            //    StringBuilder sb2 = new StringBuilder();
            //    sb2.AppendLine("Like 5 Media>");
            //    foreach (var item in x.Value.Medias.Take(5))
            //    {
            //        // like media...
            //        var liked = await InstaApi.MediaProcessor.LikeMediaAsync(item.InstaIdentifier);
            //        sb2.AppendLine($"{item.InstaIdentifier} liked? {liked.Succeeded}");
            //    }

            //    sb.AppendLine("Explore Feeds Result: " + x.Succeeded);
            //    foreach (var media in x.Value.Medias)
            //    {
            //        sb.AppendLine(DebugUtils.PrintMedia("Feed media", media));
            //    }
            //    RtBox.Text = sb2.ToString() + Environment.NewLine + Environment.NewLine + Environment.NewLine;

            //    RtBox.Text += sb.ToString();
            //    RtBox.Visible = true;
            //    Size = ChallengeSize;
            //}
        }*/

        void LoadSession()
        {
            InstaApi?.SessionHandler?.Load();

            //// Old load session
            //try
            //{
            //    if (File.Exists(StateFile))
            //    {
            //        Debug.WriteLine("Loading state from file");
            //        using (var fs = File.OpenRead(StateFile))
            //        {
            //            InstaApi.LoadStateDataFromStream(fs);
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Debug.WriteLine(ex);
            //}
        }
        void SaveSession()
        {
            if (InstaApi == null)
                return;
            if (!InstaApi.IsUserAuthenticated)
                return;
            InstaApi.SessionHandler.Save();

            //// Old save session 
            //var state = InstaApi.GetStateDataAsStream();
            //using (var fileStream = File.Create(StateFile))
            //{
            //    state.Seek(0, SeekOrigin.Begin);
            //    state.CopyTo(fileStream);
            //}
        }

    }
   

