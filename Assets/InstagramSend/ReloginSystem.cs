﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReloginSystem : MonoBehaviour
{
    [SerializeField] private GameObject _phoneUI;

    [SerializeField] private GameObject _reloginUI;

    [SerializeField] private GameObject _codeUI;

    [SerializeField] private GameObject _errorText;

    [SerializeField] private InputField _codeField;

    [SerializeField] private GameObject _succesUi;

    [SerializeField] private GameObject _processingText;
    
    private void OnEnable()
    {
        _phoneUI.SetActive(false);
        
        _errorText.SetActive(false);
        
        _succesUi.SetActive(false);
        
        _reloginUI.SetActive(true);
        
        _codeUI.SetActive(false);
        
        _processingText.SetActive(false);
    }

    public async void Relogin()
    {
        _reloginUI.SetActive(false);
                
        _processingText.SetActive(true);
        
        var result = await InstagramManager._instance.Relogin();

        _processingText.SetActive(false);

        if (result == "Phone")
        {
            _phoneUI.SetActive(true);
            _reloginUI.SetActive(false);
        }
        else
        {
            _errorText.SetActive(true);
        }
    }

    public void SendSms()
    {
        InstagramManager._instance.SendSms();
        _phoneUI.SetActive(false);
        _codeUI.SetActive(true);
    }
    

    public void SendEmail()
    {
        InstagramManager._instance.SendEmail();
        _phoneUI.SetActive(false);
        _codeUI.SetActive(true);
    }

    public async void SendCode()
    {
        _codeUI.SetActive(false);
        
        _processingText.SetActive(true);
        
        var result = await InstagramManager._instance.ResendCode(_codeField.text);
        
        _processingText.SetActive(false);

        if (result == "succes")
        {
            _succesUi.SetActive(true);
            StartCoroutine(GoodLogin());
        }
        else
        {
           _errorText.SetActive(true);
           StartCoroutine(ErrorHandle());
        }
    }

    IEnumerator ErrorHandle()
    {
        yield return new WaitForSeconds(1);
        
        _errorText.SetActive(false);
        
        _reloginUI.SetActive(true);
    }

    IEnumerator GoodLogin()
    {
        yield return new WaitForSeconds(1);
        
        gameObject.SetActive(false);
    }
}
