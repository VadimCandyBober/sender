﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LastSendedButton : MonoBehaviour
{
    public void InputButton()
    {
        KeyBoard._instance.ReChange(GetComponentInChildren<Text>().text);
    }
}
