﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowLastSendes : MonoBehaviour
{
    [SerializeField] private GameObject[] _lastUI;

    public static ShowLastSendes _instance;

    private void Awake()
    {
        _instance = this;
    }

    public void Show(string method)
    {
        
       Debug.Log("SHO LAST SENDED" + method); 
        
        for(int i  =0 ;i < _lastUI.Length  ; i ++)
            _lastUI[i].SetActive(false);
        
        var list = SendedList._instance.ItemList;
        
        List<string> _logins = new List<string>();
        
        list._sendedItems.ForEach(x =>
        {
            if (x._method == method)
            {
                if(!_logins.Contains(x._login))
                    _logins.Add(x._login);
            }
        });

        _logins.Reverse();
        
        int currentUI = 0;
        
        _logins.ForEach(login =>
        {
            if (currentUI < 5)
            {
                _lastUI[currentUI].GetComponentInChildren<Text>().text = login;
                _lastUI[currentUI].SetActive(true);
                currentUI++;
            }
        });
    }

  
}
