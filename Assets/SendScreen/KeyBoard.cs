﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyBoard : MonoBehaviour
{
    public static KeyBoard _instance;

    private string _inputedString = "";

    public string InputedString => _inputedString;
    
   // [SerializeField] private Text _inputUI;

    [SerializeField] private InputField _keyBoardInput;

    [SerializeField] private Text _buttonText;

    private bool _isPhysicalKeyBoardOn;

    private LocalizationManager _localizationManager;
    
    private void Awake()
    {
        if(_instance == null)
            _instance = this;
        else Destroy(gameObject);

        _localizationManager = FindObjectOfType<LocalizationManager>();
        
        int keyBoard = PlayerPrefs.GetInt("KeyBoard",1);
        
        if(keyBoard == 1)
            OnOffPhysical();
    }


    public void KeyInput(string key)
    {
//        Debug.Log(key);
        _inputedString += key;
        _keyBoardInput.text = _inputedString;
        
        if(key == "@")
            SavedEmailSetUp._instance.Activate();
    }

    public void ReChange(string newStr)
    {
        _inputedString = newStr;
        _keyBoardInput.text = _inputedString;
    }

    public void OnKeyBoardEditEnd()
    {
        if(_keyBoardInput.text.EndsWith("@"))
            SavedEmailSetUp._instance.Activate();
        _inputedString =  _keyBoardInput.text;
        _keyBoardInput.text = _inputedString;
    }
    

    public void OnOffPhysical()
    {
        _isPhysicalKeyBoardOn = !_isPhysicalKeyBoardOn;

        _keyBoardInput.interactable = _isPhysicalKeyBoardOn;

        if (_isPhysicalKeyBoardOn)
        {
            PlayerPrefs.SetInt("KeyBoard",1);
            PlayerPrefs.Save();
            if(_localizationManager.Language == Language.ru)
                _buttonText.text = "Выключить физическую клавиатуру";
            else _buttonText.text = "Turn off the physical keyboard";
        }
        else
        {
            PlayerPrefs.SetInt("KeyBoard",0);
            PlayerPrefs.Save();
            if(_localizationManager.Language == Language.ru)
                _buttonText.text = "Включить физическую клавиатуру";
            else _buttonText.text = "Turn on the physical keyboard";
        }
    }

    public void BackSpace()
    {
        List<Char> _inputChars = new List<char>();
        _inputChars.AddRange(_inputedString.ToCharArray());
        if(_inputChars.Count > 0)
            _inputChars.RemoveAt(_inputChars.Count - 1);
        _inputedString = "";
        _inputChars.ForEach(x => { _inputedString += x; });
        _keyBoardInput.text = _inputedString;
    }
}



