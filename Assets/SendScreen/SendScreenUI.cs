﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using RenderHeads.Media.AVProVideo;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class SendScreenUI : MonoBehaviour
{
   [SerializeField] private GameObject _sendScreen;

   [SerializeField] private GameObject _galeryScreen;

   [SerializeField] private GameObject _sendMethodScree;

   [SerializeField] private GameObject _instagramText;

   [SerializeField] private GameObject _mailText;

   [SerializeField] private GameObject _smsText;

   [SerializeField] private MediaPlayer _player;

   [SerializeField] private GameObject _mailSendedScreen;

   [SerializeField] private InputField _inputField;

   public KeyBoard _KeyBoard;

   public AddVideoToSendList _sender;

   public GameObject[] _ketBoard;

   private bool _isEnterActive;
   
   private string _sendMethod;

   public void OpenSendMethod()
   {
      _isEnterActive = false;
      _sendMethodScree.SetActive(true);
      _sendScreen.SetActive(false);
      _galeryScreen.SetActive(false);
      SendMethodsicons._instance.IconsActivate();
   }
   
   public void OpenSendScreen()
   {
      _isEnterActive = true;
      _sendMethodScree.SetActive(false);
      _sendScreen.SetActive(true);
      _galeryScreen.SetActive(false);
      _player.Stop();
   }

   public void OpenGaleryScreen()
   {
      _isEnterActive = false;
      SavedEmailSetUp._instance.Disable();
      _sendMethodScree.SetActive(false);
      _sendScreen.SetActive(false);
      _galeryScreen.SetActive(true);
   }

   IEnumerator OpenGalery()
   {
      _isEnterActive = false;
      SavedEmailSetUp._instance.Disable();
      _sendMethodScree.SetActive(false);
      _sendScreen.SetActive(false);
      _mailSendedScreen.SetActive(true);
      yield return new WaitForSeconds(1f);
      VideoPlayManager._instance.Close();
      _mailSendedScreen.SetActive(false);
      OpenGaleryScreen();
   }

   public void SendMethodSet(string method)
   {
      _sendMethod = method;
      OpenSendScreen();
      _inputField.Select();

      if (_sendMethod == "inst")
      {
         _instagramText.SetActive(true);
         _mailText.SetActive(false);
         _smsText.SetActive(false);
         KeyBoardHandle(true);
         ShowLastSendes._instance.Show("Instagram");
      }
      else if(_sendMethod == "mail")
      {
         _mailText.SetActive(true);
         _instagramText.SetActive(false);
         _smsText.SetActive(false);
         KeyBoardHandle(true);
         ShowLastSendes._instance.Show("Mail");
      } else if(_sendMethod == "sms")
      {
         _mailText.SetActive(false);
         _instagramText.SetActive(false);
         _smsText.SetActive(true);
         KeyBoardHandle(false);
          ShowLastSendes._instance.Show("SMS");
      }
   }

   public void KeyBoardHandle(bool value)
   {
      for (int i = 0; i < _ketBoard.Length; i++)
      {
         _ketBoard[i].SetActive(value);
      }
   }

   private void Update()
   {
      if(_isEnterActive)
         if (Input.GetKeyUp(KeyCode.Return))
         {
            string to = _KeyBoard.InputedString;
            Send(to);
         }
           
   }

   public async void StartSend()
   {
      StartCoroutine(OpenGalery());
      string to = _KeyBoard.InputedString;

      _KeyBoard.ReChange("");
      Debug.Log("Начало метода FactorialAsync"); // выполняется синхронно
      await Task.Run(()=>Send(to));                // выполняется асинхронно
      Debug.Log("Конец метода FactorialAsync");
   }

   public void Send(string to)
   {
      Debug.Log("START SEND");

      if (_sendMethod == "inst")
      {
         _sender.SendToInstagram(to);
      }
      else if (_sendMethod == "mail")
      {
         _sender.SendToEmail(to);
      } else if (_sendMethod == "sms")
      {
         if(!to.StartsWith("+"))
          _sender.SendToSmsm("+" + to); 
         else   _sender.SendToSmsm(to); 
      }
      
      Debug.Log("END SEND");
   }
   
}
