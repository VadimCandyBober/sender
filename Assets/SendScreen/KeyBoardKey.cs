﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyBoardKey : MonoBehaviour
{
    [SerializeField] private string _keyChar;

    public string KeyChar => _keyChar;

    [SerializeField] private Sprite _grayButton;

    [SerializeField] private Sprite _whiteButton;
    
    private DesignData _designData;

    [SerializeField] private bool _isBackSpace;

    private void OnEnable()
    {
        var json = PlayerPrefs.GetString("DesignData", "");

        if (json != "")
        {
            _designData = JsonUtility.FromJson<DesignData>(json);
        }
        else
        {
            _designData = new DesignData();
        }
      
        UIUpdate();

        if (!_isBackSpace)
        {
            GetComponent<Button>().onClick.RemoveAllListeners();
            GetComponent<Button>().onClick.AddListener(InputKey);
        }
        
          
        
    }

    private void UIUpdate()
    {
        var text = GetComponentInChildren<Text>();
        
        if (_designData._keyboard == "white")
        {
            GetComponent<Image>().sprite = _whiteButton;
          
            if(text)
            text.color = Color.black;
        }
        else
        {
            GetComponent<Image>().sprite = _grayButton;

            if(text)
            text.color = Color.white;
        }
        if(text)
            text.text = _keyChar;
    }


    public void InputKey()
    {
//        Debug.Log(_keyChar);
        KeyBoard._instance.KeyInput(_keyChar);
    }

    public void BackSpace()
    {
        KeyBoard._instance.BackSpace();
    }
    
}
