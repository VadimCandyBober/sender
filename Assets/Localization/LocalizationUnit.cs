﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class LocalizationUnit : MonoBehaviour
{
    [TextArea][SerializeField] private string _englishString;

    [TextArea][SerializeField] private string _ruString;

    private LocalizationManager _manager;

    private Text _text;

    private void OnEnable()
    {
        _text = GetComponent<Text>();

        _manager = FindObjectOfType<LocalizationManager>();
    }

    private void Update()
    {
        if (_manager != null)
        {
            if (_manager.Language == Language.en)
            {
                _text.text = _englishString;
            }
            else
            {
                _text.text = _ruString;
            } 
        }
       
    }
}
