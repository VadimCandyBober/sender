﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizationManager : MonoBehaviour
{
    public static LocalizationManager _instance;
    
    public Language Language => _language;
    
    private Language _language = Language.ru;

    [SerializeField] private Transform _selectedLanguageUI;

    [SerializeField] private Transform _enButton;

    [SerializeField] private Transform _ruButton;

    private void Awake()
    {
        Singleton();
        DontDestroyOnLoad(gameObject);

        int lang = PlayerPrefs.GetInt("lang", 1);
        
        if(lang == 1)
            SetRu();
        else SetEnglesh();
    }

    private void Singleton()
    {
        if (_instance == null)
            _instance = this;
        else Destroy(gameObject);
    }

    public void SetEnglesh()
    {
        _selectedLanguageUI.localPosition = _enButton.localPosition;
        _language = Language.en;
        SaveLanguage((int)_language);
    }

    public void SetRu()
    {
        _selectedLanguageUI.localPosition = _ruButton.localPosition;
        _language = Language.ru;
        SaveLanguage((int)_language);
    }


    private void SaveLanguage(int lang)
    {
        FindObjectOfType<SendedListUI>().TextUpdate();
        
        PlayerPrefs.SetInt("lang", lang);
        PlayerPrefs.Save();
    }

}


public enum Language
{
    en = 0,
    ru = 1
}
