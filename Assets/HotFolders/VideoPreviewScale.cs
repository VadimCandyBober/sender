﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VideoPreviewScale : MonoBehaviour
{
    [SerializeField] private GridLayoutGroup _gridLayoutGrup;

    [SerializeField] private float _maximum = 2;

    [SerializeField] private float _minimum = 0.5f;

    private float _nowPercent = 1;
    
    private Vector3 _tenPercent;

    private void Start()
    {
        _tenPercent = _gridLayoutGrup.cellSize * 0.1f;
    }

    public void Add()
    {
        if (_nowPercent < _maximum)
        {
            _nowPercent += 0.1f;
            _gridLayoutGrup.cellSize = new Vector2(_gridLayoutGrup.cellSize.x + _tenPercent.x, _gridLayoutGrup.cellSize.y + _tenPercent.y);
        }
        
        
    }

    public void Minus()
    {
        if (_nowPercent > _minimum)
        {
            _nowPercent -= 0.1f;
            _gridLayoutGrup.cellSize = new Vector2(_gridLayoutGrup.cellSize.x - _tenPercent.x, _gridLayoutGrup.cellSize.y - _tenPercent.y);
        }
       
    }
}
