﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HotFoldersUI : MonoBehaviour
{
   [SerializeField] private GameObject _hotFolderUiPrefab;

   [SerializeField] private Transform _uiParrent;
   
   private List<FolderInUI> _folders = new List<FolderInUI>();

    private HotFolder _hotFolder;

    private void Awake()
    {
        _hotFolder = FindObjectOfType<HotFolder>();
    }

    public void AddHotFolder(string path)
   {
       if (path != "")
       {
           var folder = Instantiate(_hotFolderUiPrefab, _uiParrent);
           int id = _folders.Count;
           folder.GetComponent<FolderUI>().SetUpID(id, path);
           _folders.Add(new FolderInUI(path, id , folder ));
       }
       
   }

   public void DeleteHotFolder(int id)
   {
       var folderInUI = _folders.Find(x => { return x.Id == id; });
       
       if (folderInUI != null)
       {
           Destroy(folderInUI.Folder);
           _folders.Remove(folderInUI);
           _hotFolder.Delete(folderInUI.Path);
       }
   } 
}

public class FolderInUI
{
    private string _path;

    private int _id;

    private GameObject _folder;

    public FolderInUI(string path, int id, GameObject folder)
    {
        _path = path;
        _id = id;
        _folder = folder;
    }

    public string Path => _path;

    public int Id => _id;

    public GameObject Folder => _folder;
}
