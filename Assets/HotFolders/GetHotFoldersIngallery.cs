﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GetHotFoldersIngallery : MonoBehaviour
{
    [SerializeField] private Transform _parrent;

    [SerializeField] private GameObject _hotFolderPrefab;
    
    private List<GameObject> _folders = new List<GameObject>();

    public static GetHotFoldersIngallery _instance;

    private void Awake()
    {
        _instance = this;
    }

    public bool Check(string path)
    {
        var json = PlayerPrefs.GetString("HotFolders","");

        if (json != "")
        {
            var foldersList = JsonUtility.FromJson<FoldersList>(json);

            return foldersList.Folders.Contains(path);
        }

        return false;
    }
    
    private void OnEnable()
    {
        _folders.ForEach(x => {Destroy(x);});
        
        _folders.Clear();
        
        var json = PlayerPrefs.GetString("HotFolders","");

        int counter = 0;

        if (json != "")
        {
           var  foldersList = JsonUtility.FromJson<FoldersList>(json);
            foldersList.Folders.ForEach(folder =>
            {
                if (Directory.Exists(folder))
                {
                    var data = Instantiate(_hotFolderPrefab, _parrent).GetComponent<HotFolderDataIngallery>();
                
                    _folders.Add(data.gameObject);
                
                    string nameFile = Path.GetFileName(folder);
                
                    data.SetUp(folder, nameFile, counter);

                    counter++;
                }
               
            });
        }

        StartCoroutine(FolderOpenS());
    }
    
    

    IEnumerator FolderOpenS()
    {
        if (_folders.Count == 0)
        {
            FolderOpen._instance.Clear();
            FolderOpen._instance.SetActiveSelected(false);
        } else FolderOpen._instance.SetActiveSelected(true);
        
        yield return new WaitForSeconds(0.1f);
        
        if (_folders.Count > 0)
            _folders[0].GetComponent<HotFolderDataIngallery>().FirstCLick();
      
    }
}
