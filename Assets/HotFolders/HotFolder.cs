﻿using System;
using System.Collections;
using System.Collections.Generic;
using Crosstales.FB;
using Dropbox.Api.Sharing;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class HotFolder : MonoBehaviour
{
    private FoldersList _foldersList;

    private HotFoldersUI _hotFoldersUi;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        _hotFoldersUi = FindObjectOfType<HotFoldersUI>();
        Load();

    }

    public void Add()
    {
        string path = FileBrowser.OpenSingleFolder();
       
        
        if (path != "")
        {
            _foldersList.Folders.Add(path);
        
            _hotFoldersUi.AddHotFolder(path);
        
            Save();
        }

    }

    public void Delete(string path)
    {
        _foldersList.Folders.Remove(path);
        
        Save();
    }

    private void Save()
    {
        var json = JsonUtility.ToJson(_foldersList);
        
        PlayerPrefs.SetString("HotFolders", json);
        
        PlayerPrefs.Save();
    }

    private void Load()
    {
        var json = PlayerPrefs.GetString("HotFolders","");

        if (json != "")
        {
            _foldersList = JsonUtility.FromJson<FoldersList>(json);
            _foldersList.Folders.ForEach(folder =>
            {
                _hotFoldersUi.AddHotFolder(folder);
            });
        }
        else
        {
            _foldersList = new FoldersList();
        }
    }
}

public class FoldersList
{
    [SerializeField] private  List<string> _folders = new List<string>();

    public List<string> Folders => _folders;
}

