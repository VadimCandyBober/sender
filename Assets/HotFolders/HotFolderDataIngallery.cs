﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HotFolderDataIngallery : MonoBehaviour
{
    [SerializeField] private Text _name;

    public int _id ;

    private string _path;

    public void SetUp(string path, string name, int id)
    {
        _name.text = name;
        _path = path;
        _id = id;

    }

    public void Click()
    {
        FindObjectOfType<FolderOpen>().OpenFolder(_path, transform, _name.text);
    }

    public void FirstCLick()
    {
        FindObjectOfType<FolderOpen>().FirstFolderOpen(_path, transform, _name.text);
    }
    
}
