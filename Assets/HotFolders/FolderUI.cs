﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FolderUI : MonoBehaviour
{
    private int _id;
    
    [SerializeField] private Text _path;
    
    public void SetUpID(int id, string path)
    {
        _id = id;
        _path.text = path;
    }

    public void FolderDelete()
    {
        FindObjectOfType<HotFoldersUI>().DeleteHotFolder(_id);
    }
}
