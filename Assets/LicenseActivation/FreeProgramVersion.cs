﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeProgramVersion : MonoBehaviour
{
    [SerializeField] private GameObject[] _toogles;

    [SerializeField] private GameObject[] _locksUI;

    private bool _isFree = true;

    public bool IsFree => _isFree;

    public static FreeProgramVersion _instance;

    private void Awake()
    {
        _instance = this;
    }

    public void ActivateFreeVesrison()
    {
        _isFree = true;
        
        for(int i = 0 ; i < _toogles.Length; i ++)
            _toogles[i].SetActive(false);
        
        for(int i = 0 ; i < _locksUI.Length; i ++)
            _locksUI[i].SetActive(true);
    }

    public void ActivateFullVersion()
    {
        _isFree = false;
        
        for(int i = 0 ; i < _toogles.Length; i ++)
            _toogles[i].SetActive(true);
        
        for(int i = 0 ; i < _locksUI.Length; i ++)
            _locksUI[i].SetActive(false);
    }


}
