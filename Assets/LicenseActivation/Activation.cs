﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pervasive.Data.SqlClient;
using System.IO.IsolatedStorage;
using System.Net;
using System.Net.NetworkInformation;
using SlowMo_Metro.Classes;


public class Activation : MonoBehaviour
{
   private const string connectionString = "SERVER = remotemysql.com ;DATABASE = TAqwyl7cnw; USER ID = TAqwyl7cnw, PASSWORD = wxbI0d9inA";
   
   private const string connectionString1 = "Data Source=remotemysql.com;" +
   "Initial Catalog=TAqwyl7cnw;" +
   "User id=TAqwyl7cnw;" +
   "Password=wxbI0d9inA;";

   private const string conString2 =
      "Server Name=remotemysql.com;Database Name=TAqwyl7cnw;User ID=TAqwyl7cnw;Password=wxbI0d9inA;";

   private bool Activated { get; set; }

   [SerializeField] private GameObject _errorText;


   public void IsActevated(string key,Action _onSucces, Action _onFailed)
   {
      Debug.Log(key);
      FindObjectOfType<LicenseActivate>()._activataionKey = key;
      StartCoroutine(IsActivatedCheck(key, _onSucces, _onFailed));
   }
   
   public void IsActevatedNew(string key,Action _onSucces, Action _onFailed)
   {
      Debug.Log(key);
      
      FindObjectOfType<LicenseActivate>()._activataionKey = key;
      
      StartCoroutine(IsActivatedCheckNew(key, _onSucces, _onFailed));
   }

   private void Update()
   {
//      Debug.Log(GetMacAddress());
   }

   public IEnumerator LicenceCheck(string key)
   {
      var form = new WWWForm(); 
      
      form.AddField( "unity_hash", "728079e5fd7ae643749657989a325281" );
      
      form.AddField("key", key);
      
      form.AddField("state", "activation");

      WWW www = new WWW("http://icu.by/scripts/activation_v2.php", form);
      
      yield return www;
      
      if (www.error != null)
      {
         Debug.Log(www.error);
      } else 
      {
         if (www.text == "correct")
         {
            UpdateActivation(key);
            
            using (IsolatedStorageFile isolatedStorage =  IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Assembly, null, null))
            {
               using(IsolatedStorageFileStream isolatedStorageFileStream = new IsolatedStorageFileStream("licenc1.txt", System.IO.FileMode.OpenOrCreate , isolatedStorage))
               {
                  using (System.IO.StreamWriter sw = new System.IO.StreamWriter(isolatedStorageFileStream))
                  {
                     sw.WriteLine(key);
                     sw.WriteLine("Activated");
                  }
               }
            }
            FindObjectOfType<LicenseActivate>().GoToProgram();
         }
         else
         {
            StartCoroutine(ActivatationError());
            Activated = false;
         }
         www.Dispose();
      }
   }

   IEnumerator ActivatationError()
   {
      _errorText.SetActive(true);
      yield return new WaitForSeconds(1.5f);
      _errorText.SetActive(false);
   }
   
   public IEnumerator IsActivatedCheck(string key, Action _onSucces, Action _onFailed)
   {
      var form = new WWWForm(); 
      
      form.AddField( "unity_hash", "728079e5fd7ae643749657989a325281" );
      
      form.AddField("key", key);
      
      form.AddField("state", "check");

      WWW www = new WWW("http://icu.by/scripts/activation_v2.php", form);
      
      yield return www;
      
      if (www.error != null)
      {
         Debug.Log(www.error);
         
      } else 
      {
         Debug.Log(www.text);

         Debug.Log(www.text + "--" + GetMacAddress());

         bool normalMacAdress = false;

         var macs = GetAllMacAdress();
         
         macs.ForEach(x =>
         {
            if (x == www.text)
               normalMacAdress = true;
         });
         
         if (www.text == "0"  || www.text == ""  || normalMacAdress)
         {
            if (_onFailed != null)
               _onFailed();
            
            using (IsolatedStorageFile isolatedStorage =
               IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Assembly, null, null))
            {
               if (!isolatedStorage.FileExists("licenc1.txt"))
               {
                  using(IsolatedStorageFileStream isolatedStorageFileStream = new IsolatedStorageFileStream("licenc1.txt", System.IO.FileMode.OpenOrCreate , isolatedStorage))
                  {
                     using (System.IO.StreamWriter sw = new System.IO.StreamWriter(isolatedStorageFileStream))
                     {
                        sw.WriteLine(key);
                        sw.WriteLine("Activated");
                     }
                  }
               }
                  
            }
         }
         else 
         {
            Debug.Log("SUCCES");
            
            if (_onSucces != null)
               _onSucces();

           
         }
         www.Dispose();
      }
   }
   
   public IEnumerator IsActivatedCheckNew(string key, Action _onSucces, Action _onFailed)
   {
      var form = new WWWForm(); 
      
      form.AddField( "unity_hash", "728079e5fd7ae643749657989a325281" );
      
      form.AddField("key", key);
      
      form.AddField("state", "check");

      WWW www = new WWW("http://icu.by/scripts/activation_v2.php", form);
      
      yield return www;
      
      if (www.error != null)
      {
         Debug.Log(www.error);
         
      } else 
      {
         Debug.Log(www.text);

         Debug.Log(www.text + "--" + GetMacAddress());
         
         if (www.text == "0"  || www.text == "")
         {
            if (_onFailed != null)
               _onFailed();
            
         }
         else 
         {
            if (_onSucces != null)
               _onSucces();

           
         }
         www.Dispose();
      }
   }

   public void CheckDelete(string key, Action _onSucces)
   {
      StartCoroutine(CheckToDelete(key, _onSucces));
   }
   
   public IEnumerator CheckToDelete(string key, Action _onSucces)
   {
      var form = new WWWForm(); 
      
      form.AddField( "unity_hash", "728079e5fd7ae643749657989a325281" );
      
      form.AddField("key", key);
      
      form.AddField("state", "check");

      WWW www = new WWW("http://icu.by/scripts/activation_v2.php", form);
      
      yield return www;
      
      if (www.error != null)
      {
         Debug.Log(www.error);
         
      } else 
      {
         Debug.Log(www.text);

         Debug.Log(www.text + "--" + GetMacAddress());
         
         if (www.text == "Data invalid - cant find name.")
         {
           
            if (_onSucces != null)
               _onSucces();
         }
       
         www.Dispose();
      }
   }
   

   public string GetMacAddress()
   {
      NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
      String sMacAddress = string.Empty;
      foreach (NetworkInterface adapter in nics)
      {
         if (sMacAddress == String.Empty)// only return MAC Address from first card  
         {
            IPInterfaceProperties properties = adapter.GetIPProperties();
            sMacAddress = adapter.GetPhysicalAddress().ToString();
         }
      } return sMacAddress;
   }
   
   public static string GetExternalIPAddress()
   {
      using (WebClient wc = new WebClient()) 
      { 
         try
         {
            string externalip = new WebClient().DownloadString("http://icanhazip.com");
            return externalip;
         }
         catch (Exception)
         {
            return null;
         }
      }
   }

   public List<string> GetAllMacAdress()
   {
      List<string> macsAdress = new List<string>();
      
      NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
      foreach (NetworkInterface adapter in nics)
      {
         macsAdress.Add(adapter.GetPhysicalAddress().ToString());
      }

      return macsAdress;
   }
   
   
   
   public IEnumerator LicenceUpdate(string key)
   {
      var form = new WWWForm();

      form.AddField("key", key);
      form.AddField("unity_hash", "728079e5fd7ae643749657989a325281");
      form.AddField("state", "update");

      form.AddField("mac", GetMacAddress().ToString());
      form.AddField("motherBoardId", SysInfoLic.GetMotherBoard_ID());
      form.AddField("hddSerial", SysInfoLic.GetHddSerial());
      form.AddField("md5", GetMacAddress());
      form.AddField("programm", "Event Media Saver");
      form.AddField("date", DateTime.Now.ToString());
      form.AddField("ip", GetExternalIPAddress());

      WWW www = new WWW("http://icu.by/scripts/activation_v2.php", form);
      
      yield return www;
      
      if (www.error != null)
      {
         Debug.Log(www.error);
      } else 
      {
         if (www.text == "correct")
         {
            Debug.Log("You  soft has been activated");
         }
         else
         {
            Debug.Log("Error");

         }
         www.Dispose();
      }
   }
   
   public void ActivateSoftware(string key)
   {
      IsActevated(key, null, () => StartCoroutine(LicenceCheck(key)));
   }

   private void UpdateActivation(string key)
   {
      StartCoroutine(LicenceUpdate(key));
   }
}
