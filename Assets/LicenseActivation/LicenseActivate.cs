﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using Dropbox.Api.TeamLog;
using UnityEngine;
using UnityEngine.UI;

public class LicenseActivate : MonoBehaviour
{
   public static LicenseActivate _instance;

   private bool _isProgramActivated = false;

   private Activation _activation;

   [SerializeField] private GameObject _activationCanvas;

   [SerializeField] private GameObject _mainCanvas;

   [SerializeField] private InputField _firstScreenFiled;

   [SerializeField]private bool _activated;

   [SerializeField] private GameObject _activatedWindow;

   [SerializeField] private GameObject _activateWindow;

   [SerializeField] private Text _activationKeyText;

   [HideInInspector]public string _activataionKey;
   
   //-----------------------------

   [SerializeField] private InputField _inGameActivationField;

   private string _key;

   public static string Clipboard
   {
      get { return GUIUtility.systemCopyBuffer; }
      set { GUIUtility.systemCopyBuffer = value; }
   }

   private void Awake()
   {
      
      _instance = this;

      _activation = FindObjectOfType<Activation>();

      Load();
   }

   public void FirstScreenActiavation()
   {
      _activation.ActivateSoftware(_firstScreenFiled.text);
   }

   public void ActivationProgram(string key)
   {
      _activation.ActivateSoftware(key);
   }

   public void InGameActivation()
   {
      _activation.ActivateSoftware(_inGameActivationField.text);
   }

   public void ActivationTest()
   {
      _activation.ActivateSoftware("14052");
   }


   public void Paste()
   {
      try
      {
         string toPaste = Clipboard;
         _firstScreenFiled.text = toPaste;
      }
      catch (Exception e)
      {
         Debug.Log("No to paste");
      }
   }
   
   public void PasteInGame()
   {
      try
      {
         string toPaste = Clipboard;
         _inGameActivationField.text = toPaste;
      }
      catch (Exception e)
      {
         Debug.Log("No to paste");
      }
   }

   IEnumerator LicenceCheck()
   {
      var form = new WWWForm(); 
      form.AddField( "unity_hash", "hashcode" );
      form.AddField("key", 14052);

      WWW www = new WWW("http://icu.by/scripts/activation.php", form);
      
      yield return www;
      
      if (www.error != null) {
         Debug.Log(www.error);
      } else {
         Debug.Log("Test ok");
         Debug.Log(www.text);
         www.Dispose();
      }
   }
   
   

   public void Load()
   {
      using (IsolatedStorageFile isolatedStorageFile =  IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Assembly, null, null))
      {
         try
         {
            using (IsolatedStorageFileStream isolatedStorageFileStream = new IsolatedStorageFileStream("licenc1.txt",FileMode.Open, isolatedStorageFile))
            {
               using (StreamReader sr = new StreamReader(isolatedStorageFileStream))
               {

                  string key = sr.ReadLine();
               
                  Debug.Log("ACTIVATED");
                  
                  if (sr.ReadLine() == "Activated")
                  {
                     _activated = true;
                     GoToProgram();
                     FindObjectOfType<FreeProgramVersion>().ActivateFullVersion();
                  }
                  
                  FindObjectOfType<Activation>().IsActevatedNew(key,GoToProgram, GoToActivation);

                  _key = key;
               }
            }
         }
         catch (Exception e)
         {
            Debug.Log("BAAAD");
         }
      }
   }

   public void CheckToDelete()
   {
      try
      {
         FindObjectOfType<Activation>().CheckDelete(_key, () =>
         {
            using (IsolatedStorageFile isolatedStorageFile =
               IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Assembly, null, null))
            {
               try
               {
                  isolatedStorageFile.DeleteFile("licenc1.txt");
                  Debug.Log("DELETE");
               }
               catch
               {
                  Debug.Log("Error delete licens");
               }
            }

         });
      }
      catch (Exception e)
      {
        Debug.Log("First");
      }
    
   }

   public void GoToActivation()
   {
      _mainCanvas.SetActive(false);
      
      _activationCanvas.SetActive(true);
   }

   public void GoToProgram()
   {
      _activated = true;
      
      _mainCanvas.SetActive(true);
      
      _activationCanvas.SetActive(false);
      
      if(_activateWindow.activeSelf)
      {
         _activatedWindow.SetActive(true);
         _activateWindow.SetActive(false);
      }
      
      FindObjectOfType<FreeProgramVersion>().ActivateFullVersion();
   }

   public void GoToFreeVersion()
   {
      _mainCanvas.SetActive(true);
      
      _activationCanvas.SetActive(false);
      
      FindObjectOfType<FreeProgramVersion>().ActivateFreeVesrison();
   }

   public void GoToACtivationTab()
   {
      _activatedWindow.SetActive(false);
      _activateWindow.SetActive(false);
      
      if(_activated)
         _activatedWindow.SetActive(true);
      else _activateWindow.SetActive(true);

      _activationKeyText.text = _activataionKey;
   }
   
   


   IEnumerator CheckLicense()
   {
      yield return null;
   }
}
