﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using MediaFilesManager;
using MediaFilesManager.Abstractions;
using MediaToolkit;
using MediaToolkit.Model;
using MediaToolkit.Options;
using UnityEngine;


public class ThumbnailsTest : MonoBehaviour
{
    public string CreateThumbnail(string inValue, double inSeconds)
    {
        var inputFile = new MediaFile { Filename = @inValue };
        string outputFileName = inValue.Substring(0,inValue.LastIndexOf("."))  + ".jpg";
        var outputFile = new MediaFile { Filename = @outputFileName };

        using (var engine = new Engine())
        {
            engine.GetMetadata(inputFile);
            var options = new ConversionOptions { Seek = TimeSpan.FromSeconds(inSeconds) };
            if (!System.IO.File.Exists(outputFileName))
                engine.GetThumbnail(inputFile, outputFile, options);
        }
        
        return outputFileName;
    }
}
