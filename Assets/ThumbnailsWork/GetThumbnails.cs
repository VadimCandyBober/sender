﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using MediaToolkit;
using MediaToolkit.Model;
using MediaToolkit.Options;
using Debug = UnityEngine.Debug;

public static class GetThumbnails 
{
   public static async Task<Texture2D> GetThumbanils(string path)
   {
      Texture2D texture = new Texture2D(128, 128);
      
      try
      {
         var ffMpeg = new NReco.VideoConverter.FFMpegConverter();

         string filename = Application.dataPath + "/" + Path.GetFileName(path) + ".png";

         if(!File.Exists(filename))
            ffMpeg.GetVideoThumbnail(path,filename,5);

         byte[] fileData;
 
         if (File.Exists(filename))     
         {
            fileData = File.ReadAllBytes(filename);
            texture.LoadImage(fileData); 
         }
      }
      catch (Exception e)
      {
         return texture;
      }
     
      return texture;
   }
   
   public static Texture2D GetThumbnail(string path)
   {
      Texture2D texture = new Texture2D(240, 240);
      
      string filename = Application.dataPath + "/" + Path.GetFileName(path) + ".png";
      
      var cmd = "ffmpeg  -itsoffset -1  -i " + '"' + path + '"' + " -vcodec mjpeg -vframes 1 -an -f rawvideo -s 320x240 " + '"' + filename + '"';

      var startInfo = new ProcessStartInfo
      {
         WindowStyle = ProcessWindowStyle.Hidden,
         FileName = "cmd.exe",
         Arguments = "/C " + cmd
      };

      var process = new Process
      {
         StartInfo = startInfo
      };

      process.Start();

      process.WaitForExit(5000);

      byte[] fileData;
 
      if (File.Exists(filename))     
      {
         fileData = File.ReadAllBytes(filename);
         texture.LoadImage(fileData); 
      }

      return texture;
   }

   public static Texture2D GetThumbnailsNew(string path)
   {
      Texture2D texture = new Texture2D(128, 128);
      
      var inputFile = new MediaFile { Filename = @path };
      
      //string filename = Application.dataPath + "/thumd" + Path.GetFileName(path) + ".png";
      
     string outputFileName = path.Substring(0,path.LastIndexOf("."))  + ".jpg";
      var outputFile = new MediaFile { Filename = @outputFileName };

      using (var engine = new Engine())
      {
         engine.GetMetadata(inputFile);
         var options = new ConversionOptions { Seek = TimeSpan.FromSeconds(5) };
         if (!System.IO.File.Exists(outputFileName))
            engine.GetThumbnail(inputFile, outputFile, options);
      }
      
        
      byte[] fileData;
 
      if (File.Exists(outputFileName))     
      {
         fileData = File.ReadAllBytes(outputFileName);
         texture.LoadImage(fileData); 
      }

      return texture;
   }
   
}
