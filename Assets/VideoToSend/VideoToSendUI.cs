﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class VideoToSendUI : MonoBehaviour
{
    private List<VideoToSendData> _data = new List<VideoToSendData>();

    public static VideoToSendUI _instance;

    [SerializeField] private GameObject _videoPrefab;

    [SerializeField] private Transform _parrent;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else Destroy(gameObject);
    }


    public void Add(string _path, Sprite _image, PreviewObject previewObjectToSend)
    {
        var  previewObject = Instantiate(_videoPrefab, _parrent).GetComponent<PreviewObject>();

        previewObject._lastPreviewObject = previewObjectToSend;
        
        if(Path.GetExtension(_path) == ".gif" ||Path.GetExtension(_path) == ".GIF" )
            previewObject.SetImageToPreview(_image, _path,true);
        else previewObject.SetImage(_image, _path);

        previewObject._toogle = true;

        previewObject._can = false;
        
        previewObject._toogleObj.SetActive(true);
        
        _data.Add(new VideoToSendData(previewObject.gameObject, _path));
    }

    public void Remove(string _path)
    {
        VideoToSendData dataToremove = null;
        
        _data.ForEach(data =>
        {
            if (data._path == _path)
            {
                Destroy(data._uiObject);
                dataToremove = data;
            }
        });


        if (dataToremove != null)
            _data.Remove(dataToremove);
    }
   
}

[System.Serializable]
public class VideoToSendData
{
    public GameObject _uiObject;

    public string _path;


    public VideoToSendData(GameObject uiObject, string path)
    {
        _uiObject = uiObject;
        _path = path;
    }
}
