﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SendMethodsicons : MonoBehaviour
{
    public static SendMethodsicons _instance;

    [SerializeField] private GameObject _instIcon;

    [SerializeField] private GameObject _mailIcon;
    
    [SerializeField] private GameObject _smsIcon;

    [SerializeField] private GameObject _allOffText;
    
    private void Awake()
    {
        _instance = this;
    }


    public void IconsActivate()
    {
        AllOff();

        SendKindsData _data = null;
        
        var dataJson = PlayerPrefs.GetString("SendKinds", "");

        if (dataJson != "")
        {
            _data = JsonUtility.FromJson<SendKindsData>(dataJson);
        }
        else
        {
            _data =  new SendKindsData();
        }

        bool isAllOff = true;

        if (_data._instagram || _data._mail || _data._telegram)
            isAllOff = false;

        if (isAllOff)
        {
            _allOffText.SetActive(true);
        }
        
        if(_data._instagram)
            _instIcon.SetActive(true);
        
        if(_data._telegram)
            _smsIcon.SetActive(true);
        
        if(_data._mail)
            _mailIcon.SetActive(true);
    }

    private void AllOff()
    {
        _instIcon.SetActive(false);
        _mailIcon.SetActive(false);
        _allOffText.SetActive(false);
        _smsIcon.SetActive(false);
    }
}
