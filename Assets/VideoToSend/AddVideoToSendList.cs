﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;


public class AddVideoToSendList : MonoBehaviour
{
   public static AddVideoToSendList _instance;
   
   [SerializeField]private List<string> _filesToSend = new List<string>();

   [SerializeField] private GameObject _infoBox;

   [SerializeField] private Text _infoText;

   private LocalizationManager _localizationManager;

   public MailSendManager _MailSendManager;

   public InstagramManager _InstagramManager;

   public SmsManager _SmsManager;

   public UnityMainThreadDispatcher _Dispatcher;
   
   private void Awake()
   {
      if (_instance == null)
         _instance = this; 
      else Destroy(gameObject);

      _localizationManager = FindObjectOfType<LocalizationManager>();
   }

   public void Add(string path, Sprite image, PreviewObject previewObject, int lenght)
   {
      string extensions = Path.GetExtension(path);

      if (extensions == ".mov" || extensions == ".mp4"|| extensions == ".MP4" || extensions == ".MOV")
      {
         
         if (lenght < 60)
         {
            if (FreeProgramVersion._instance.IsFree)
            {
               long length = new System.IO.FileInfo(path).Length / (1000 * 1024);
               if (length < 2.1f)
               {
                  _filesToSend.Add(path);
                  VideoToSendUI._instance.Add(path,image, previewObject);
                  if(_localizationManager.Language == Language.ru)
                     StartCoroutine(ShowInfoBox("Видео успешно добавлено"));
                  else StartCoroutine(ShowInfoBox("Video added successfully"));
               }
               else
               {
                  if(_localizationManager.Language == Language.ru)
                     StartCoroutine(ShowInfoBox("Видео превышает лимит по размеру бесплатной версии. Купите лицнзию"));
                  else StartCoroutine(ShowInfoBox("The video exceeds the limit in size of the free version. Buy a license"));
                  
               }
         
               Debug.Log(length);
            }
            else
            {
               if(_localizationManager.Language == Language.ru)
                  StartCoroutine(ShowInfoBox("Видео успешно добавлено"));
               else StartCoroutine(ShowInfoBox("Video added successfully"));
               _filesToSend.Add(path);
               VideoToSendUI._instance.Add(path,image, previewObject);
            }
         }
         else
         {
            if(_localizationManager.Language == Language.ru)
               StartCoroutine(ShowInfoBox("Превышен лимит в 60 секунд"));
            else StartCoroutine(ShowInfoBox("60 seconds limit exceeded"));
         }
      }
      else
      {
         if(_localizationManager.Language == Language.ru)
            StartCoroutine(ShowInfoBox("Изображение успешно добавлено"));
         else StartCoroutine(ShowInfoBox("Image added successfully"));
         _filesToSend.Add(path);
         VideoToSendUI._instance.Add(path,image, previewObject);
      }
      
      
      
    
   }

   public IEnumerator ShowInfoBox(string message)
   {
      _infoBox.SetActive(true);
      _infoText.text = message;
      
      yield return new WaitForSeconds(2);
      
      _infoBox.SetActive(false);
   }

   public void RemoveFileToSend(string path)
   {
      try
      {
         _filesToSend.Remove(path);
      
         VideoToSendUI._instance.Remove(path);

         var previewObjects = FindObjectsOfTypeAll(typeof(PreviewObject)).ToList();

         for (int i = 0; i < previewObjects.Count; i++)
         {
            var prev = previewObjects[i] as PreviewObject;
         
            if(prev._path == path) 
               prev.Off();
         }
      }
      catch (Exception e)
      {
         Debug.Log(e);
      }
     
   }

   public void SendToEmail(string to)
   {
      Mail(to);
   }

   public void SendToSmsm(string to)
   {
      SMS(to);
   }

   private void SMS(string to)
   {
      List<string> copy = new List<string>();
      
      copy.AddRange(_filesToSend);
      
      int count = _filesToSend.Count;

      _Dispatcher.Enqueue(() =>
      {
         for (int i = 0; i < count; i++)
         {
            RemoveFileToSend(_filesToSend[0]);
         }
      });
      
      copy.ForEach(async x =>
      {
         await _SmsManager.SendSms(to, x);
      });
   }

   public bool CanAdd()
   {
      return _filesToSend.Count < 3;
   }

   private void Mail(string to)
   {
      List<string> copy = new List<string>();
      
      copy.AddRange(_filesToSend);
      
      int count = _filesToSend.Count;

      _Dispatcher.Enqueue(() =>
      {
         for (int i = 0; i < count; i++)
         {
            RemoveFileToSend(_filesToSend[0]);
         }
      });
      
      copy.ForEach(async x =>
      {
         await _MailSendManager.SendEmail(to, x);
      });

     
   }


   public void SendToInstagram(string to)
   {
      Inst(to);
   }

   private void Inst(string to)
   {
      List<string> copy = new List<string>();
      
      copy.AddRange(_filesToSend);
      int count = _filesToSend.Count;
      
      _Dispatcher.Enqueue(() =>
      {
         for (int i = 0; i < count; i++)
         {
            RemoveFileToSend(_filesToSend[0]);
         }
      });
      
      copy.ForEach(async x =>
      {
         var path  = x.Replace("file://", "");

         string fileExtension = Path.GetExtension(path);

         await _InstagramManager.SendDirectImage(to, path);
      });
   }

  

}


