﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainScripts : MonoBehaviour
{
   [SerializeField]
   private GameObject _galeryCanvas;
   
   [SerializeField]
   private GameObject _adminCanvas;

   private int clickCount = 0;

   private void Awake()
   {
       //Screen.SetResolution(1280, 720, FullScreenMode.Windowed);
   }

   public void LoadNextScene()
   {
       LicenseActivate._instance.CheckToDelete();
       _galeryCanvas.SetActive(true);
       _adminCanvas.SetActive(false);
   }

    public void LoadAdmin()
    {
        clickCount++;
        StartCoroutine(ClickReset());
        if (clickCount >= 2)
        {
            _galeryCanvas.SetActive(false);
            _adminCanvas.SetActive(true);
        }
                
    }

    public void Exit()
    {
        Application.Quit();
    }

    IEnumerator ClickReset()
    {
        yield return new WaitForSeconds(1);

        clickCount = 0;
    }
}
