﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using UnityEngine;
using InstagramApiSharp;
using InstagramApiSharp.API.Builder;
using InstagramApiSharp.Classes;


public class InstApiTest : MonoBehaviour
{
    private InstagramApiSharp.API.IInstaApi _instaApi;
    
    private void Start()
    {
        Login();
    }

    private void Update()
    {
       if(Input.GetKeyUp(KeyCode.Space))
       {
          Debug.Log(_instaApi.IsUserAuthenticated);
           
           
       }
    }

    private async void Login()
    {
        var userSession = new UserSessionData
        {
            UserName = "vadim@candybober.games",
            Password = "3edccde3"
        };

            _instaApi = InstaApiBuilder.CreateBuilder()
            .SetUser(userSession)
            .Build();

        if (!_instaApi.IsUserAuthenticated)
        {
            Debug.Log($"Logging in as {userSession.UserName}");
            var logInResult = await _instaApi.LoginAsync();
            if (!logInResult.Succeeded)
            {
                Debug.Log($"Unable to login: {logInResult.Info.Message}");
                return;
            }
        }
        
        var desireUsername = "vadus";
        var user = await _instaApi.UserProcessor.GetUserAsync(desireUsername);
        var userId = user.Value.Pk.ToString();
        var directText = await _instaApi.MessagingProcessor
            .SendDirectTextAsync(userId, null, "Hello!!");

    }
}
