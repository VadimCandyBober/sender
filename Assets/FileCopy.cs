﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Crosstales.FB;
using UnityEditor;
using UnityEngine;

public class FileCopy : MonoBehaviour
{
	private string _pathToCopy = "";

	[SerializeField] private float _fileScanTickRate = 5;

	private float _timeToNewScal;

	private LoadedFiles _loadedFiles;

	private string _path;

	private bool _startScanFolder;

	private void Awake()
	{
		string loadedFilesJson = PlayerPrefs.GetString("LoadedFiles");

		if (loadedFilesJson != "")
		{
			_loadedFiles = JsonUtility.FromJson<LoadedFiles>(loadedFilesJson);
		}
		else
		{
			_loadedFiles = new LoadedFiles();
		}
	}

	void Start()
    {
		#if UNITY_IPHONE
    	_pathToCopy = Application.dataPath + "/Raw;
    	#elif UNITY_ANDROID
    	_pathToCopy = "jar:file://" + Application.dataPath + "!/assets";
    	#else
	    _pathToCopy = Application.dataPath + "/StreamingAssets";
		#endif 
    }

	private void Update()
	{
		if (_startScanFolder)
		{
			if (_timeToNewScal < Time.time)
			{
				ScanFiles();
				_timeToNewScal = Time.time + _fileScanTickRate;
			}
		}
	}

	public void AddHotFolder()
	{
		_path = FileBrowser.OpenSingleFolder("Select folder");

		_startScanFolder = true;
	}

	public void ScanFiles()
	{
		string[] files;

		string pathPreFix = @"file://";
        
		files = System.IO.Directory.GetFiles(_path, "*.png");

		for (int i = 0; i < files.Length; i++)
		{
			if (!_loadedFiles._filesNames.Contains(files[i]))
			{
				StartCoroutine(LoadFile(pathPreFix + files[i], files[i].Replace(_path,"")));
			}
		}
	}
	
	public IEnumerator LoadFile(string path, string fileName)
	{	
		string savePath = string.Format("{0}/{1}", _pathToCopy, fileName);

		WWW www = new WWW(path);
		
		yield return www;
		
		File.WriteAllBytes(savePath, www.bytes);

		if (www.error == null)
		{
			_loadedFiles._filesNames.Add(fileName);

			string json = JsonUtility.ToJson(_loadedFiles);
			
			PlayerPrefs.SetString("LoadedFiles",json);
			
			PlayerPrefs.Save();
		}
	}
	
	
}

[System.Serializable]
public class LoadedFiles
{
	public List<string> _filesNames = new List<string>();
}
