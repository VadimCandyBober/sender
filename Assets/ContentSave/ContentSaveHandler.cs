﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class ContentSaveHandler : MonoBehaviour
{
    private ContentSaveData _contentSave;

    private bool _stated;
    
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        
        UpdateStatus();
        
    }

    public void UpdateStatus()
    {
        var json = PlayerPrefs.GetString("SaveData", "");

        if (json != "")
        {
            _contentSave = JsonUtility.FromJson<ContentSaveData>(json);
        }
        else
        {
            _contentSave = new ContentSaveData();
        }

        if (_contentSave._active && !_stated)
            ContentSaveHandle();
    }

    public async void ContentSaveHandle()
    {
        try
        {
            _stated = true;
        
            string distPath = Path.Combine(_contentSave._destPath);

            string syncPath = Path.Combine(_contentSave._syncPath);

            if (distPath != "" && syncPath != "" && Directory.Exists(distPath) && Directory.Exists(syncPath)) 
            {
                var extImage = new List<string> {".jpg", ".png",".mp4", ".mov", ".MOV",".JPG", ".PNG",".MP4", ".jpeg", ".JPEG", ".gif", ".GIF"};
            
                var _movFilesSync =  Directory.GetFiles(syncPath, "*.*")
                    .Where(s => extImage.Contains(Path.GetExtension(s))).ToList();

                var _movFilesInDist =  Directory.GetFiles(distPath, "*.*")
                    .Where(s => extImage.Contains(Path.GetExtension(s))).ToList();
        
                List<string> syncFiles = new List<string>();
        
                List<string> distFiles = new List<string>();

                foreach (var file in _movFilesSync)
                {
                    string text = file;

                    string name = Path.GetFileName(text);
                
                    syncFiles.Add(name);
                }
        
                foreach (var file in _movFilesInDist)
                {
                    string text = file;

                    string name = Path.GetFileName(text);
                
                    distFiles.Add(name);
                }

                for (int i = 0; i < syncFiles.Count; i++)
                {
                    string fileName = syncFiles[i];
                
                    if (!distFiles.Contains(fileName))
                    {
                        try
                        {
                            if(Directory.Exists(distPath) && Directory.Exists(syncPath))
                                await Copy(syncPath, fileName, distPath);
                        }
                        catch (Exception e)
                        {
                            Debug.Log(e);
                        }
                    }
                }

            }

            await Task.Delay(2000);

            if (_contentSave._active)
                ContentSaveHandle();
            else _stated = false;

        }
        catch (Exception e)
        { 
            await Task.Delay(10000);
            ContentSaveHandle();
            Debug.Log(e);
        }
    }

    public async Task Copy(string syncPath, string fileName, string distPath)
    {
        try
        {
            if(Directory.Exists(distPath) && Directory.Exists(syncPath))
                await CopyFileAsync(Path.Combine(syncPath, fileName), Path.Combine(distPath, fileName));

            Debug.Log("VIDEO SAVE");

            await Task.Delay(2000);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
       
    }
        
    public async Task CopyFileAsync(string sourcePath, string destinationPath)
    {
        if (sourcePath != null)
            using (Stream source = File.Open(sourcePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (Stream destination = File.Create(destinationPath))
                {
                    await source.CopyToAsync(destination);
                    
                    destination.Close();
                    
                    destination.Dispose();
                }
                
                source.Close();
                    
                source.Dispose();
            }
    }    
    
}
