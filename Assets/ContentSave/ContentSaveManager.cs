﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Crosstales.FB;
public class ContentSaveManager : MonoBehaviour
{
    public static ContentSaveManager _instance;
    
    [SerializeField] private Text _destPathText;

    [SerializeField] private Text _syncPathText;

    [SerializeField] private Sprite _on;

    [SerializeField] private Sprite _off;

    [SerializeField] private Image _toogle;
    
    private ContentSaveData _contentSave;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else Destroy(gameObject);

        var json = PlayerPrefs.GetString("SaveData", "");

        if (json != "")
        {
            _contentSave = JsonUtility.FromJson<ContentSaveData>(json);
        }
        else
        {
            _contentSave = new ContentSaveData();
        }
        
        UiUpdate();
    }

    public void UiUpdate()
    {
        _destPathText.text = _contentSave._destPath;
        
        _syncPathText.text = _contentSave._syncPath;

        if (_contentSave._active)
            _toogle.sprite = _on;
        else _toogle.sprite = _off;
    }

    public void SetToogle()
    {
        _contentSave._active = !_contentSave._active;
        Save();
        UiUpdate();
        FindObjectOfType<ContentSaveHandler>().UpdateStatus();
    }
    

    public void SetDestPath()
    {
        _contentSave._destPath = FileBrowser.OpenSingleFolder("Select folder");
        Save();
        UiUpdate();
        FindObjectOfType<ContentSaveHandler>().UpdateStatus();
    }

    public void SetSyncPath()
    {
       _contentSave._syncPath =  FileBrowser.OpenSingleFolder("Select folder");
       Save();
       UiUpdate();
       FindObjectOfType<ContentSaveHandler>().UpdateStatus();
    }

    private void Save()
    {
        PlayerPrefs.SetString("SaveData", JsonUtility.ToJson(_contentSave));
        PlayerPrefs.Save();
    }

}

[System.Serializable]
public class ContentSaveData
{
    public string _syncPath;

    public string _destPath;

    public bool _active;
}

