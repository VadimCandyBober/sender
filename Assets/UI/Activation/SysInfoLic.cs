﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UnityEngine;

namespace SlowMo_Metro.Classes
{
    public static class SysInfoLic
    {
        public static string GetHddSerial()
        {
            try
            {
                ArrayList hdCollection = new ArrayList();

                ManagementObjectSearcher searcher = new
                    ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");

                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    HardDrive hd = new HardDrive();
                    hd.Model = wmi_HD["Model"].ToString();
                    hd.Type = wmi_HD["InterfaceType"].ToString();

                    hdCollection.Add(hd);
                }



                searcher = new
                    ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia");

                int i = 0;
                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    try
                    {
                        // get the hard drive from collection
                        // using index
                        HardDrive hd = (HardDrive)hdCollection[i];
                        // get the hardware serial no.
                        if (wmi_HD["SerialNumber"] == null)
                            hd.SerialNo = "None";
                        else
                            hd.SerialNo = wmi_HD["SerialNumber"].ToString();

                        i++;
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                
                }

                // Display available hard drives
                foreach (HardDrive hd in hdCollection)
                {
                    Console.WriteLine("Model\t\t: " + hd.Model);
                    Console.WriteLine("Type\t\t: " + hd.Type);
                    Console.WriteLine("Serial No.\t: " + hd.SerialNo);
                    Console.WriteLine();
                }
                int ccount = hdCollection.Count;

                HardDrive[] hdArray = hdCollection.ToArray(typeof(HardDrive)) as HardDrive[];
                return hdArray[0].SerialNo;
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }

            return "not support on this platform";

        }
        
        public static string GetMotherBoard_ID()
        {
            try
            {
                string MotherBoardID = string.Empty;
                SelectQuery query = new SelectQuery("Win32_BaseBoard");
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);


                ManagementObjectCollection.ManagementObjectEnumerator enumerator = searcher.Get().GetEnumerator();
                while (enumerator.MoveNext())
                {
                    ManagementObject info = (ManagementObject)enumerator.Current;
                    MotherBoardID = info["SerialNumber"].ToString().Trim();
                }
                return MotherBoardID;
            }
            catch (Exception e)
            {
               Debug.Log(e);
            }
            return "not support on this platform";
        }
        
}

    internal class HardDrive
    {
        public string Model;
        public string Type;
        public string  SerialNo;
    }
}
