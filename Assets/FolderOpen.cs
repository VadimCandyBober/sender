﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using RenderHeads.Media.AVProVideo;
using UnityEditor;
using UnityEngine;
using UnityEngine.Video;
using Color = UnityEngine.Color;
using Graphics = UnityEngine.Graphics;
using Image = UnityEngine.UI.Image;
using Random = System.Random;

public class FolderOpen : MonoBehaviour
{
    private string path;
    
    [SerializeField]private List<string> files = new List<string>();

    [SerializeField]private List<string>  _movFiles = new List<string>();
    
    [SerializeField]private List<string> _gifIFles = new List<string>();
    
    Texture2D[] textList;

    [SerializeField] private GameObject _prewievPrefab;

    [SerializeField] private Transform _canvasParrent;

    //private List<GameObject> _prefabs = new List<GameObject>();

    [SerializeField] private GameObject _selectedHotFolder;

    HotFolders selectedFolder = null;
    
    private List<HotFolders> _hotFolders = new List<HotFolders>();

    private string _pathSended;
    
    private Transform _buttonPos;
    
    private string _name;

    private bool _isSortActive;

    private string _path;

    public static FolderOpen _instance;

    [SerializeField] private MediaPlayer _player;

    public bool _gifEnded, _imageEnded;

    private void Awake()
    {
        _instance = this;
        ThumbsDelete();
    }

    private void Update()
    {
        if(_buttonPos != null)
            _selectedHotFolder.transform.position = _buttonPos.position;
        
    }

    public void SetActiveSelected(bool value)
    {
        _selectedHotFolder.SetActive(value);
    }

    public void Clear()
    {
               
        StopAll();
        
        _hotFolders.ForEach(x =>
        {
            x._instantiatedPreviews.ForEach(y =>
            {
                Destroy(y._gameObject);
            });
        });
        
        _hotFolders.Clear();
 
    }
    
    private void ThumbsDelete()
    {
        try
        {
            var extVideos = new List<string> {".png"};
        
            var thumbs =  Directory.GetFiles(Application.streamingAssetsPath, "*.*")
                .Where(s => extVideos.Contains(Path.GetExtension(s))).ToList();
        
            thumbs.ForEach(thumb =>
            {
                try
                {
                    string fileName = Path.GetFileName(thumb);
                    
                    DateTime creation = File.GetCreationTime(thumb);
                
                    Debug.Log(creation);

                    if(Math.Abs((DateTime.Now-creation).TotalDays) >= 6)
                        File.Delete(thumb);

                }
                catch (Exception e)
                {
                    Debug.Log(e);
                    Debug.Log("THUMB NOT DELETED");
                }
            });
        }
        catch (Exception e)
        {
           Debug.Log(e);
        }
        
    }

    public void OpenFolder(string pathSended, Transform buttonPos, string name, bool _needClose = true)
    {
        try
        {
            var json = PlayerPrefs.GetString("HotFolders","");

            int count = 0;

            if (json != "")
            {
                var  foldersList = JsonUtility.FromJson<FoldersList>(json);

                count = foldersList.Folders.Count;
            }
                
            
            if (Directory.Exists(pathSended) && count > 0)
            {
                selectedFolder = null;
        
                StopCoroutine("LoadMovies");
        
                StopCoroutine("LoadImages");
            
                StopCoroutine("LoadGIF");

                _pathSended = Path.Combine(pathSended);

                _buttonPos = buttonPos;
        
                _name = name;

                if(_needClose)
                    VideoPlayManager._instance.Close();
        
                path = pathSended;
        
                _path = Path.Combine(pathSended);
        
                _hotFolders.ForEach(x =>
                {
                    x._instantiatedPreviews.ForEach(prefab =>
                    {
                        prefab._gameObject.SetActive(false);
                    });
                });
        
                _hotFolders.ForEach(x =>
                {
                    if (x._folderName == name)
                        selectedFolder = x;
                });

                if (selectedFolder == null)
                {
                    selectedFolder = new HotFolders();
                    selectedFolder._path = _pathSended;
                    selectedFolder._folderName = name;
                    _hotFolders.Add(selectedFolder);
                }
        
                StartCoroutine("LoadImages", selectedFolder);
            
                StartCoroutine("LoadGIF", selectedFolder);

                StartCoroutine("LoadMovies", selectedFolder);
            }
        }
        catch (Exception e)
        {
            Debug.Log("YEP");
        }
       
      
    }
    public void StopAll()
    {
        
        _player.m_VideoPath = "";
        
        StopCoroutine("LoadMovies");
        
        StopCoroutine("LoadImages");
        
        StopCoroutine("LoadGIF");
    }
    public void RestartALl()
    {
        OpenFolder(_pathSended,_buttonPos,_name, false);
    }
    public void FirstFolderOpen(string pathSended, Transform buttonPos, string name, bool _needClose = true)
    {
        try
        {
            var json = PlayerPrefs.GetString("HotFolders","");

            int count = 0;

            if (json != "")
            {
                var  foldersList = JsonUtility.FromJson<FoldersList>(json);

                count = foldersList.Folders.Count;
            }

            if (Directory.Exists(pathSended) && count > 0)
            {
                bool checkedFolder = false;
            
                if (selectedFolder != null)
                {
                    checkedFolder = GetHotFoldersIngallery._instance.Check(selectedFolder._path);
                }
            
                if (selectedFolder == null || !checkedFolder || _buttonPos == null)
                {
                    StopCoroutine("LoadMovies");
        
                    StopCoroutine("LoadImages");
                
                    StopCoroutine("LoadGIF");
        
                    // _selectedHotFolder.SetActive(true);

                    _pathSended = pathSended;
        
                    _buttonPos = buttonPos;
        
                    _name = name;

                    if(_needClose)
                        VideoPlayManager._instance.Close();

                    path = pathSended;

                    _hotFolders.ForEach(x =>
                    {
                        x._instantiatedPreviews.ForEach(prefab =>
                        {
                            prefab._gameObject.SetActive(false);
                        });
                    });
        
                    _hotFolders.ForEach(x =>
                    {
                        if (x._folderName == name)
                            selectedFolder = x;
                    });

                    if (selectedFolder == null)
                    {
                        selectedFolder = new HotFolders();
                
                        selectedFolder._folderName = name;

                        selectedFolder._path = pathSended;
                
                        _hotFolders.Add(selectedFolder);
                    }
         
                    StartCoroutine("LoadImages", selectedFolder);
        
                    StartCoroutine("LoadMovies", selectedFolder);
                
                    StartCoroutine("LoadGIF", selectedFolder);
                }
            
            }

        }
        catch (Exception e)
        {
            Debug.Log("YEP");
        }
       
    }
    
    private IEnumerator LoadMovies(HotFolders hotFolders)
    {

        var extVideos = new List<string> {".mp4", ".mov", ".MP4", ".MOV"};
        try
        {
            if(Directory.Exists(path))
                _movFiles =  Directory.GetFiles(path, "*.*")
                .Where(s => extVideos.Contains(Path.GetExtension(s))).ToList();
        }
        catch (Exception e)
        {
            _movFiles.Clear();
            Debug.Log(e);
        }

        if (!Directory.Exists(path))
        {
            _movFiles.Clear();
        }
           

        int dummy = 0;
        
        hotFolders._instantiatedPreviews.ForEach(x =>
        {
            x._gameObject.SetActive(true);
        });
        
        yield return new WaitForEndOfFrame();

        int threadCount = 0 ;

        bool _error = false;
        
        if(Directory.Exists(path))
            foreach(string tstring in _movFiles)
        {
            if (!hotFolders._loadedFiles.Contains(tstring) && Directory.Exists(path) && File.Exists(tstring))
            {
                yield return new WaitForEndOfFrame();

                string fileName = Path.GetFileName(tstring);
                
                var str2 = fileName.Split('.')[0];
                
                int circleCounter = 0;
                
                string pathTemp = tstring;
                
                if (!File.Exists(Application.persistentDataPath + "/" + str2 + "thumb" + ".png") && Directory.Exists(path) )
                {
                    try
                    {
                        if (Directory.Exists(path))
                        {
                            _player.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, tstring, false);

                            _player.Stop();
                        }
                    }
                    catch
                    {
                        _error = true;
                    }

                    if (!_error)
                    {
                        while ((_player.Info.GetVideoWidth() < 10 || _player.TextureProducer.GetTexture(0) == null) &&
                               circleCounter < 500 && Directory.Exists(path))
                        {
                            yield return new WaitForSeconds(0.01f);
                            circleCounter++;
                        }
                    }
                }

                if (!_error)
                {
                    yield return new WaitForEndOfFrame();

                if (circleCounter < 490 && Directory.Exists(path))
                {
                    try
                    {
                        Texture2D texture = new Texture2D(240, 240);

                        if(!File.Exists(Application.persistentDataPath + "/" + str2 + "thumb" + ".png") && Directory.Exists(path))
                        {       
                            Texture mainTexture = _player.TextureProducer.GetTexture(
                                0);
                            Texture2D texture2D =
                                new Texture2D(mainTexture.width, mainTexture.height, TextureFormat.RGBA32, false);

                            RenderTexture currentRT = RenderTexture.active;

                            RenderTexture renderTexture = new RenderTexture(mainTexture.width, mainTexture.height, 32);

                            Graphics.Blit(mainTexture, renderTexture);

                            RenderTexture.active = renderTexture;

                            texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);

                            texture2D.Apply();

                            Color[] pixels = texture2D.GetPixels();

                            RenderTexture.active = currentRT;

                            var scaledTexture = ScaleTexture(texture2D, texture2D.width / 4, texture2D.height / 4);
                            
                            File.WriteAllBytes(Application.persistentDataPath + "/" + str2 + "thumb" + ".png", scaledTexture.EncodeToPNG());
                            
                            texture = texture2D;
                            
                        }
                        else
                        {
                            Debug.Log("READ");
                                    
                            var textureBytesd = File.ReadAllBytes(Application.persistentDataPath + "/" + str2 + "thumb" + ".png");

                            texture.LoadImage(textureBytesd);
                        }
                        

                        _player.m_VideoPath = "";

                        var previewObject = Instantiate(_prewievPrefab, _canvasParrent).GetComponent<PreviewObject>();

                        previewObject.SetImage(texture.ConvertToSprite(), pathTemp);

                        hotFolders._loadedFiles.Add(tstring);

                        DateTime createDate = new System.IO.FileInfo(pathTemp).CreationTime;

                        var instObj = new InstantiatedItem(previewObject.gameObject, createDate);

                        hotFolders._instantiatedPreviews.Add(instObj);

                    }
                    catch (Exception e)
                    {
                        Debug.Log(e);
                    }

                    yield return new WaitForEndOfFrame();
                }
                }
            }
        }

        try
        {
            if (Directory.Exists(path))
            {
                List<string> deletedFromLoaded = new List<string>();

                hotFolders._loadedFiles.ForEach(x =>
                {
                    if (!_movFiles.Contains(x))
                    {
                        var prevObject = hotFolders._instantiatedPreviews.Where(y =>
                        {
                            return y._gameObject.GetComponent<PreviewObject>()._path == x;
                        }).ToList()[0];

                        if (prevObject != null)
                        {
                            hotFolders._instantiatedPreviews.Remove(prevObject);
                            Destroy(prevObject._gameObject);
                            deletedFromLoaded.Add(x);

                        }
                    }
                });

                deletedFromLoaded.ForEach(x => { hotFolders._loadedFiles.Remove(x); });

                Sort();
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);

        }
        
        yield return new WaitForSeconds(2);

        while (!_gifEnded || !_imageEnded)
        {
            Debug.Log("CHIIL");
            yield return new WaitForSeconds(0.1f);
        }
        
        if(Directory.Exists(_pathSended))
            OpenFolder(_pathSended,_buttonPos,_name, false);

    }
    private Texture2D ScaleTexture(Texture2D source,int targetWidth,int targetHeight) {
        Texture2D result=new Texture2D(targetWidth,targetHeight,source.format,true);
        Color[] rpixels=result.GetPixels(0);
        float incX=((float)1/source.width)*((float)source.width/targetWidth);
        float incY=((float)1/source.height)*((float)source.height/targetHeight);
        for(int px=0; px<rpixels.Length; px++) {
            rpixels[px] = source.GetPixelBilinear(incX*((float)px%targetWidth),
                incY*((float)Mathf.Floor(px/targetWidth)));
        }
        result.SetPixels(rpixels,0);
        result.Apply();
        return result;
    }

    
  
   
    
    private IEnumerator LoadImages(HotFolders hotFolders)
    {
        _imageEnded = false;
        Debug.Log("IMAGE LOAD");
        var extImage = new List<string> {".jpg", ".png", ".JPG", ".PNG", ".jpeg", ".JPEG"};

        try
        {
            if(Directory.Exists(path))
                files =  Directory.GetFiles(path, "*.*")
                .Where(s => extImage.Contains(Path.GetExtension(s))).ToList();
        }
        catch (Exception e)
        {
            files.Clear();
            Debug.Log(e);
        }
        
        if(!Directory.Exists(path))
            files.Clear();
        
        foreach(string tstring in files)
        {
            if (!hotFolders._loadedImages.Contains(tstring) && Directory.Exists(path) && File.Exists(tstring))
            {
                string pathTemp = "file:///" + tstring;

                string fileName = Path.GetFileName(tstring);
                
                var str2 = fileName.Split('.')[0];
                
                WWW www = null;
                
                if (!File.Exists(Application.persistentDataPath + "/" + str2 + "thumb" + ".png"))
                {
                    www = new WWW(pathTemp);

                    yield return www;
                }

                Texture2D texTmp = new Texture2D(1024, 1024, TextureFormat.RGB24, false);
                
                try
                {
                    if (!File.Exists(Application.persistentDataPath + "/" + str2 + "thumb" + ".png"))
                    {
                        if (www != null)
                        {
                            www.LoadImageIntoTexture(texTmp);
                            
                            var scaledTexture = ScaleTexture(texTmp, texTmp.width / 2, texTmp.height / 2);
                            
                            File.WriteAllBytes(Application.persistentDataPath + "/" + str2 + "thumb" + ".png", scaledTexture.EncodeToPNG());
                        }
                       
                    }
                    else
                    {
                        Debug.Log("READ");
                                    
                        var textureBytesd = File.ReadAllBytes(Application.persistentDataPath + "/" + str2 + "thumb" + ".png");

                        texTmp.LoadImage(textureBytesd);
                    }


                    var previewObject = Instantiate(_prewievPrefab, _canvasParrent).GetComponent<PreviewObject>();

                    previewObject.SetImage(texTmp.ConvertToSprite(), pathTemp, texTmp.ConvertToSpriteDefault());
                
                    hotFolders._loadedImages.Add(tstring);

                    DateTime createDate =  new System.IO.FileInfo(tstring).CreationTime;
                    
                    var instObj = new InstantiatedItem(previewObject.gameObject, createDate);
                
                    hotFolders._instantiatedPreviews.Add(instObj);
                   
                    var trToscale = instObj._gameObject.GetComponent<PreviewObject>()._previewImage.transform;
                    
                    trToscale.localScale = new Vector3(  trToscale.localScale.x,  trToscale.localScale.y* -1, trToscale.localScale.z);
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }
            }
        }
        
        try
        {
            if (Directory.Exists(path))
            {
                List<string> deletedFromLoaded =new List<string>();
            
                hotFolders._loadedImages.ForEach(x =>
                {
                    if (!files.Contains(x))
                    {
                        var prevObjects = hotFolders._instantiatedPreviews.Where(y =>
                        {
                            var path = y._gameObject.GetComponent<PreviewObject>()._path.Replace("file:///", "");
                            
                            return path ==  x;
                        }).ToList();

                        if (prevObjects.Count > 0)
                        {
                            var prevObject = prevObjects[0];
                            if (prevObject != null)
                            {
                                hotFolders._instantiatedPreviews.Remove(prevObject);
                                Destroy(prevObject._gameObject);
                                deletedFromLoaded.Add(x);
                            }
                        }
                    }
                });
            
                deletedFromLoaded.ForEach(x => { hotFolders._loadedImages.Remove(x); });
            
                Sort();
            }

        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

        Debug.Log("IMAGE LOAD END");
        _imageEnded = true;

    }
    
    private IEnumerator LoadGIF(HotFolders hotFolders)
    {
        _gifEnded = false;
        Debug.Log("GIF LOAD");
        var extImage = new List<string> {".gif", ".GIF"};

        try
        {
            if (Directory.Exists(path))
                _gifIFles = Directory.GetFiles(path, "*.*")
                    .Where(s => extImage.Contains(Path.GetExtension(s))).ToList();
        }
        catch (Exception e)
        {
            _gifIFles.Clear();
            Debug.Log(e);
        }

        if(!Directory.Exists(path))
            _gifIFles.Clear();
        
        foreach(string tstring in _gifIFles)
        {
            if (!hotFolders._loadedGifs.Contains(tstring) && Directory.Exists(path) && File.Exists(tstring))
            {

                string pathTemp = "file:///" + tstring;

                string fileName = Path.GetFileName(tstring);
                
                var str2 = fileName.Split('.')[0];
                
                Texture2D texTmp = new Texture2D(1024, 1024, TextureFormat.RGB24, false);
                
                if (!File.Exists(Application.persistentDataPath + "/" + str2 + "thumb" + ".png"))
                {

                    using (WWW www = new WWW(pathTemp))
                    {
                        yield return www;
                        
                       

                        List<UniGif.GifTexture> m_gifTextureList = new List<UniGif.GifTexture>();

                        yield return StartCoroutine(UniGif.GetTextureListCoroutine(www.bytes,
                            (gifTexList, loopCount, width, height) =>
                            {
                                Debug.Log("LOAD GIFS 4 " + pathTemp);
                                if (gifTexList != null)
                                {
                                    m_gifTextureList = gifTexList;
                                    if (m_gifTextureList.Count > 0)
                                        texTmp = gifTexList[0].m_texture2d;
                                }
                                else
                                {
                                    Debug.LogError("Gif texture get error.");
                                }
                            },
                            FilterMode.Bilinear, TextureWrapMode.Clamp, false));
                    }
                }

                try
                {
                    if (!File.Exists(Application.persistentDataPath + "/" + str2 + "thumb" + ".png"))
                    {

                        var scaledTexture = ScaleTexture(texTmp, texTmp.width / 2 , texTmp.height / 2);
                            
                        File.WriteAllBytes(Application.persistentDataPath + "/" + str2 + "thumb" + ".png", scaledTexture.EncodeToPNG());
                    }
                    else
                    {

                        var textureBytesd = File.ReadAllBytes(Application.persistentDataPath + "/" + str2 + "thumb" + ".png");

                        texTmp.LoadImage(textureBytesd);
                    }
                    
                    var previewObject = Instantiate(_prewievPrefab, _canvasParrent).GetComponent<PreviewObject>();

                    previewObject.SetImage(texTmp.ConvertToSprite(), pathTemp, texTmp.ConvertToSpriteDefault());
                
                    hotFolders._loadedGifs.Add(tstring);

                    DateTime createDate =  new System.IO.FileInfo(tstring).CreationTime;
                    
                    var instObj = new InstantiatedItem(previewObject.gameObject, createDate);
                
                    hotFolders._instantiatedPreviews.Add(instObj);

                    var trToscale = instObj._gameObject.GetComponent<PreviewObject>()._previewImage.transform;
                    
                    trToscale.localScale = new Vector3(  trToscale.localScale.x,  trToscale.localScale.y* -1, trToscale.localScale.z);
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }
            }
        }
        
        try
        {
            if (Directory.Exists(path))
            {
                List<string> deletedFromLoaded =new List<string>();
            
                hotFolders._loadedGifs.ForEach(x =>
                {
                    if (!_gifIFles.Contains(x))
                    {
                        var prevObjects = hotFolders._instantiatedPreviews.Where(y =>
                        {
                            var path = y._gameObject.GetComponent<PreviewObject>()._path.Replace("file:///", "");

                            return path ==  x;
                        }).ToList();

                        if (prevObjects.Count > 0)
                        {
                            var prevObject = prevObjects[0];
                            if (prevObject != null)
                            {
                                hotFolders._instantiatedPreviews.Remove(prevObject);
                                Destroy(prevObject._gameObject);
                                deletedFromLoaded.Add(x);
                            }
                        }
                        
                    }
                });
            
                deletedFromLoaded.ForEach(x => { hotFolders._loadedGifs.Remove(x); });
            
                Sort();
            }

        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

        Debug.Log("GIF LOAD END");
        
        _gifEnded = true;
    }

    public void SortActivate()
    {
        _isSortActive = !_isSortActive;
        
        Sort();
    }

    public void Sort()
    {
        if (_isSortActive)
        {
            GFG gg = new GFG(); 
        
            selectedFolder._instantiatedPreviews.Sort(gg); 
        }
        else
        {
            GFGUnder gg = new GFGUnder(); 
        
            selectedFolder._instantiatedPreviews.Sort(gg); 
        }


        int number = 0;
        
        selectedFolder._instantiatedPreviews.ForEach(folder =>
        {
            folder._gameObject.transform.SetSiblingIndex(number);
            number++;
//             Debug.Log(folder._createDate);
        });

    }
}

class GFG : IComparer<InstantiatedItem> 
{ 
    public int Compare(InstantiatedItem x, InstantiatedItem y) 
    {
        // CompareTo() method 
        return y._createDate.CompareTo(x._createDate); 
          
    } 
} 

class GFGUnder : IComparer<InstantiatedItem> 
{ 
    public int Compare(InstantiatedItem x, InstantiatedItem y) 
    {
        // CompareTo() method 
        return x._createDate.CompareTo(y._createDate); 
          
    } 
} 

public class HotFolders
{
    public string _folderName;

    public string _path;
    
    public List<string> _loadedFiles = new List<string>(); 
    
    public List<string> _loadedImages = new List<string>();
    
    public List<string> _loadedGifs = new List<string>();
    
    public List<InstantiatedItem> _instantiatedPreviews = new List<InstantiatedItem>();
}


public class InstantiatedItem
{
    public GameObject _gameObject;

    public DateTime _createDate;

    public InstantiatedItem(GameObject obj, DateTime createDate)
    {
        _gameObject = obj;
        _createDate = createDate;
    }
}
