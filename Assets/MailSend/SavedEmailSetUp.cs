﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SavedEmailSetUp : MonoBehaviour
{
    private SavedEmails _emails;

    public GameObject[] _emailsUIs;

    [SerializeField] private GameObject _ui;

    public static SavedEmailSetUp _instance;

    private void Awake()
    {
        _instance = this;
    }

    public void Activate()
    {
        _ui.SetActive(true);
        
        var json = PlayerPrefs.GetString("SavedEmails", "");

        if (json != "")
        {
            _emails = JsonUtility.FromJson<SavedEmails>(json);
        }
        else
        {
            _emails = new SavedEmails();
        }
        
        UiSetUp();
    }

    public void Disable()
    {
        _ui.SetActive(false);
    }


    private void UiSetUp()
    {
        int currentEmail = 0;
           
        _emails._savedEmails.ForEach(x =>
        {
            if (x != "-1")
            {
                _emailsUIs[currentEmail].SetActive(true);
                _emailsUIs[currentEmail].GetComponentInChildren<Text>().text = x;
            }
            else
            {
                _emailsUIs[currentEmail].SetActive(false);
            }

            currentEmail++;

        }); 
    }
}
