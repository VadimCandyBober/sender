﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SavedMails : MonoBehaviour
{
    [SerializeField]private SavedEmails _emails;

    [SerializeField] private InputField _email1;

    [SerializeField] private InputField _email2;
    
    [SerializeField] private InputField _email3;
    
    [SerializeField] private InputField _email4;
    
    [SerializeField] private InputField _email5;

    private void Start()
    {
        var json = PlayerPrefs.GetString("SavedEmails", "");

        if (json != "")
        {
            _emails = JsonUtility.FromJson<SavedEmails>(json);
        }
        else
        {
            _emails = new SavedEmails();
        }
        
        UiUpdate();
    }

    void UiUpdate()
    {
        if( _emails._savedEmails[0] != "-1")
        _email1.text = _emails._savedEmails[0];
        if( _emails._savedEmails[1] != "-1")
        _email2.text = _emails._savedEmails[1];
        if( _emails._savedEmails[2] != "-1")
        _email3.text = _emails._savedEmails[2];
        if( _emails._savedEmails[3] != "-1")
        _email4.text = _emails._savedEmails[3];
        if( _emails._savedEmails[4] != "-1")
        _email5.text = _emails._savedEmails[4];
    }

    public void SerFirst()
    {
        _emails._savedEmails[0] = _email1.text;
        Save();
    }
    
    public void SerSecond()
    {
        _emails._savedEmails[1] = _email2.text;
        Save();
    }
    
    public void SerThird()
    {
        _emails._savedEmails[2] = _email3.text;
        Save();
    }
    
    public void SerFourth()
    {
        _emails._savedEmails[3] = _email4.text;
        Save();
    }
    
    public void SerFive()
    {
        _emails._savedEmails[4] = _email5.text;
        Save();
    }

    private void Save()
    {
        Debug.Log("Test");
        PlayerPrefs.SetString("SavedEmails", JsonUtility.ToJson(_emails));
        PlayerPrefs.Save();
    }
}

[System.Serializable]
public class SavedEmails
{
    public List<string> _savedEmails = new List<string>();

    //public List<string> SavedEmails1 => _savedEmails;

    public SavedEmails()
    {
        for(int i = 0 ; i < 5; i ++)
        _savedEmails.Add("-1");
    }
}
