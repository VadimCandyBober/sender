﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.Threading.Tasks;
using Dropbox.Api.TeamLog;
using UnityEngine;
using UnityEngine.UI;

public class MailSendManager : MonoBehaviour
{
    public static MailSendManager _instance;
    
    [SerializeField] private InputField _serverInput;

   // [SerializeField] private InputField _fromInput;

    [SerializeField] private InputField _mailSubjectInput;

    [SerializeField] private InputField _mailBodyInput;

    [SerializeField] private InputField _serverPortInput;

    [SerializeField] private InputField _loginInput;

    [SerializeField] private InputField _passInput;

    [SerializeField] private InputField _testMailInput;

    [SerializeField] private MailParams _mailParams;

    [SerializeField] private Image _toogle;

    [SerializeField] private Sprite[] _toogleStates;

    private SendedList _sendedList;

    private UnityMainThreadDispatcher _Dispatcher;

    private GoogleDriveTest _drive;
 
    private void Awake()
    {
        _Dispatcher = FindObjectOfType<UnityMainThreadDispatcher>();
        _drive = FindObjectOfType<GoogleDriveTest>();
        _sendedList = FindObjectOfType<SendedList>();
        if (_instance == null)
            _instance = this;
        else Destroy(gameObject);

        string json = PlayerPrefs.GetString("MailParams", "");

        if (json != "")
            _mailParams = JsonUtility.FromJson<MailParams>(json);
        else _mailParams = new MailParams();
        
        UISetUp();

        if (!Directory.Exists(Application.streamingAssetsPath + "/Test"))
            Directory.CreateDirectory(Application.streamingAssetsPath + "/Test");

       string[] files = Directory.GetFiles(Application.streamingAssetsPath + "/Test");
       
       for(int i = 0 ; i < files.Length ; i ++)
           File.Delete(files[i]);
    }

    private void UISetUp()
    {
        _serverInput.text = _mailParams._server;
       // _fromInput.text = _mailParams._from;
        _mailSubjectInput.text = _mailParams._mailSubject;
        _mailBodyInput.text = _mailParams._mailBody;
        _serverPortInput.text = _mailParams._serverPort.ToString();
        _loginInput.text = _mailParams._login;
        _passInput.text = _mailParams._pass;
        if (_mailParams._sendToGD)
            _toogle.sprite = _toogleStates[0];
        else _toogle.sprite = _toogleStates[1];

    }

    private void Save()
    {
        PlayerPrefs.SetString("MailParams", JsonUtility.ToJson(_mailParams));
    }

    public void ServerSet()
    {
        _mailParams._server = _serverInput.text;
        Save();
    }

    public void SetGD()
    {
        _mailParams._sendToGD = !_mailParams._sendToGD;
        UISetUp();
        Save();
    }

    public void FromSet()
    {
       // _mailParams._from = _fromInput.text;
        Save();
    }
    

    public void SubjectSet()
    {
        _mailParams._mailSubject = _mailSubjectInput.text;
        Save();
    }

    public void BodySey()
    {
        _mailParams._mailBody = _mailBodyInput.text;
        Save();
    }

    public void SetverPortSet()
    {
        _mailParams._serverPort = System.Convert.ToInt32(_serverPortInput.text);
        Save();
    }

    public void LoginSet()
    {
        _mailParams._login = _loginInput.text;
        Save();
    }

    public void PassSet()
    {
        _mailParams._pass = _passInput.text;
        Save();
    }

    public void SendTestWithImage()
    {
        SendEmail(_testMailInput.text);
    }
    

    public async Task SendEmail(string to, string path = null)
    {
        try
        {
            Debug.Log("MAAAAIL");
        
            string fileName = "-1";

            string time = "-1";

            
            if (path != null)
            {
                if (path.StartsWith("file"))
                {
                    path = path.Replace("file:///", "");
                    Debug.Log(path);
                }

                fileName = Path.GetFileName(path);

                time = DateTime.Now.ToString();
                _Dispatcher.Enqueue(() =>
                {
                    _sendedList.AddToItemList(
                        new SendedItem("Mail",to, "NO",fileName, time, path));
                });
                
            }
        
            MailMessage mail = new MailMessage();
        
            SmtpClient SmtpServer = new SmtpClient(_mailParams._server);

            mail.From = new MailAddress(_mailParams._login);
            mail.To.Add(to);
            mail.Subject = _mailParams._mailSubject;
            mail.Body = _mailParams._mailBody + " \n";

            if (path != null)
            {
                if (!_mailParams._sendToGD)
                {
                    string newPath = Path.Combine(path);

                    string copyPath = Application.streamingAssetsPath + "/Test/temp" + fileName;

                    if (!File.Exists(copyPath))
                        await CopyFileAsync(newPath, copyPath);

                    Attachment attachment = new Attachment(copyPath);
                    
                    string text = path;

                    string Name = Path.GetFileName(newPath);
                    
                    attachment.Name = Name;
                    
                    mail.Attachments.Add(attachment);
                }
                else
                {
                    string url = await _drive.GoogleDriveUploadToMail(path);

                    if (url != "-1")
                    {
                       mail.Body += url;
                    }
                }
          
            }
            
            SmtpServer.Port = _mailParams._serverPort;
            
            SmtpServer.Credentials = new System.Net.NetworkCredential(_mailParams._login, _mailParams._pass);
            
            SmtpServer.EnableSsl = true;
            
            try
            {
                 await SmtpServer.SendMailAsync(mail);
            }
            catch (Exception e)
            {
                Debug.Log(e);
                return;
            }

            _Dispatcher.Enqueue(() => { _sendedList.UpdateStat("Mail", to, fileName, time); });

        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }
    
    public async Task CopyFileAsync(string sourcePath, string destinationPath)
    {
        if (sourcePath != null)
            using (Stream source = File.Open(sourcePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (Stream destination = File.Create(destinationPath))
                {
                    await source.CopyToAsync(destination);
                    
                    destination.Close();
                    
                    destination.Dispose();
                }
                
                source.Close();
                    
                source.Dispose();
            }
    }    

   

    public async Task ResendEmail(string to, string path, string fileName, string time)
    {
        try
        {
            MailMessage mail = new MailMessage();
        
            SmtpClient SmtpServer = new SmtpClient(_mailParams._server);

            mail.From = new MailAddress(_mailParams._login);
            
            mail.To.Add(to);
            
            mail.Subject = _mailParams._mailSubject;
            
            mail.Body = _mailParams._mailBody;
        
            if (path != null)
            {
                if (path.StartsWith("file"))
                {
                    path = path.Replace("file:///", "");
                   // Debug.Log(path);
                }

                
                if (!_mailParams._sendToGD)
                {
                    string newPath = Path.Combine(path);

                    string copyPath = Application.streamingAssetsPath + "/Test/temp" + fileName;
                    
                    if(!File.Exists(copyPath))
                        File.Copy(newPath, copyPath);

                    Attachment attachment = new Attachment(copyPath);
                    
                    string text = path;

                    string Name = Path.GetFileName(newPath);
                    
                    attachment.Name = Name;
                    
                    mail.Attachments.Add(attachment);
                }
                else
                {
                    string url = await _drive.GoogleDriveUploadToMail(path);

                    if (url != "-1")
                    {
                        mail.Body += url;
                    }
                }
            }

            SmtpServer.Port = _mailParams._serverPort;
            
            SmtpServer.Credentials = new System.Net.NetworkCredential(_mailParams._login, _mailParams._pass);
            
            SmtpServer.EnableSsl = true;
            
            try
            {
                await SmtpServer.SendMailAsync(mail);
            }
            catch (Exception e)
            {
                Debug.Log(e);
                return;
            }

            _Dispatcher.Enqueue(() => { _sendedList.UpdateStat("Mail", to, fileName, time); });
           
          
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
       
    }
    
    private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
    {
        if (e.Error == null)
        {
            MailParamsCallBack mail = (MailParamsCallBack) e.UserState;
        
            SendedList._instance.UpdateStat("Mail", mail._to, mail._name,mail._time);
        }
     
    }


    /* public void SendMail(string to, string path)
    {
        StartCoroutine(SendEmail(to, path));
    }*/

    public void SendTestMail()
    {
        SendEmail(_testMailInput.text);
    }
}

[System.Serializable]
public class MailParamsCallBack
{
public string _name;

public string _to;

public string _time;

public MailParamsCallBack(string name, string to, string time)
{
    _name = name;
    _to = to;
    _time = time;
}
}

[System.Serializable]
public class MailParams
{  
public string _server = "smtp.yandex.com";

public string _mailSubject = "";

public string _mailBody = "";

public int _serverPort = 587;

public string _login = "";

public string _pass = "";

public bool _sendToGD = false;


}
