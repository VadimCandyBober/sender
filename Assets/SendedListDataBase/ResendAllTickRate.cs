﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class ResendAllTickRate : MonoBehaviour
{
    [SerializeField] private Sprite _on;

    [SerializeField] private Sprite _off;

    private bool _tickRateOn;

    private bool _isStarted;

    [SerializeField] private Image[] _image;

    [SerializeField] private InputField[] _timeFields;

    public static ResendAllTickRate _instance;

    private int resendTime;

    private Task _task;

    private MailSendManager _mailSendManager;

    private InstagramManager _instagramManager;

    private SmsManager _smsManager;

    private void Awake()
    {
        _mailSendManager = FindObjectOfType<MailSendManager>();
        _instagramManager = FindObjectOfType<InstagramManager>();
        _smsManager = FindObjectOfType<SmsManager>();
        _instance = this;
    }
 
    private void Start()
    {
        if (PlayerPrefs.GetInt("Tick", 0) == 1)
            _tickRateOn = true;

        resendTime = PlayerPrefs.GetInt("TimeResend", 5);
        
        FiledUpdate();

        if (_tickRateOn)
        {
            for(int i = 0 ; i < _image.Length; i ++)
                _image[i].sprite = _on;
            if(!_isStarted)
                StartResend();
               //StartCoroutine("ResendAll"); 
        }
        else  for(int i = 0 ; i < _image.Length; i ++)
            _image[i].sprite = _off;
        
    }

    private void FiledUpdate()
    {
        for (int i = 0; i < _timeFields.Length; i++)
        {
            _timeFields[i].text = resendTime.ToString();
        }
    }

    public void SetTime(int newTime)
    {
        resendTime = newTime;
        
        FiledUpdate();
        
        PlayerPrefs.SetInt("TimeResend",resendTime);
        PlayerPrefs.Save();
    }

    public void ChangeValue()
    {
        _tickRateOn = !_tickRateOn;
        if (_tickRateOn)
        {
            for(int i = 0 ; i < _image.Length; i ++)
                _image[i].sprite = _on;
            if (!_isStarted)
                StartResend();
                //StartCoroutine("ResendAll"); 
            PlayerPrefs.SetInt("Tick",1);
            PlayerPrefs.Save();
        }
        else
        {
           // StopCoroutine("ResendAll");
           StopResend();
            for(int i = 0 ; i < _image.Length; i ++)
                _image[i].sprite = _off;
            PlayerPrefs.SetInt("Tick",0);
            PlayerPrefs.Save();
        }
    }

    public async void StartResend()
    {
        Debug.Log("Начало метода FactorialAsync");
        // выполняется синхронно
        var list = FindObjectOfType<SendedList>().ItemList;
        _task = Task.Run(()=> ResendTick(list));
        await _task;
        Debug.Log("Конец метода FactorialAsync");
    }

    public void StopResend()
    {
        _task.Dispose();
    }

    public async Task ResendTick(SendedItemsList list)
    {
        await Task.Delay(10000);
        
        _isStarted = true;

      

        if (list != null)
        {
            await list._sendedItems.ForEachAsync(async item =>
            {
                if (item._sended == "NO")
                {
                    DateTime time = DateTime.Parse(item._time);

                    Debug.Log((DateTime.Now - time).TotalMinutes);
                
                    if ((DateTime.Now - time).TotalMinutes > 10)
                    {
                        if (item._method == "Mail")
                        {
                            await _mailSendManager.ResendEmail(item._login,item._path,item._fileName, item._time);
                        }
                        else if(item._method == "Instagram")
                        {
                            string fileExtension = Path.GetExtension(item._path);
     
                            if(fileExtension == ".mov" || fileExtension == ".mp4")
                                await _instagramManager.ReSendDirectImage(item._login,item._path,item._time,item._fileName);
                            else await _instagramManager.ReSendDirectImage(item._login,item._path,item._time,item._fileName);
                        } else if(item._method == "SMS")
                        {
                            await _smsManager.ResendSms(item._login, item._path, item._time, item._fileName);
                        }
                    }
                }
            
            });
        }
      
        await Task.Delay(resendTime * 60 * 1000);
    }
    
}
