﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Crosstales.FB;
using UnityEditor;
using UnityEngine;

public class SendedList : MonoBehaviour
{
    public static SendedList _instance;
    
    private SendedItemsList _itemList;

    public SendedItemsList ItemList => _itemList;
    
    private static string reportSeparator = ";";

    private MailSendManager _mailSendManager;

    private InstagramManager _instagramManager;

    private UnityMainThreadDispatcher _dispatcher;
   
    
    private static string[] reportHeaders = new string[5] {
        "Method",
        "Login",
        "Sended",
        "FileName",
        "Date"
    };

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else Destroy(gameObject);
        
        DontDestroyOnLoad(gameObject);
        
        _mailSendManager = FindObjectOfType<MailSendManager>();
        _instagramManager = FindObjectOfType<InstagramManager>();
        _dispatcher = FindObjectOfType<UnityMainThreadDispatcher>();
    }

    private void Start()
    {
        var json = PlayerPrefs.GetString("ItemList", "");

        if (json != "")
        {
            _itemList = JsonUtility.FromJson<SendedItemsList>(json);
            FindObjectOfType<SendedListUI>().UiUpdate(_itemList);
        }
        else
        {
            _itemList = new SendedItemsList();
        }
    }

    public void AddToItemList(SendedItem sendedItem)
    {
        _itemList._sendedItems.Add(sendedItem);
        FindObjectOfType<SendedListUI>().Add(sendedItem);
        Save();
    }

    public void UpdateStat(string method, string login, string fileName, string time)
    {
        _itemList._sendedItems.ForEach(item =>
        {
            if (item._method == method && item._login == login && item._fileName == fileName && item._time == time)
            {
                item._sended = "YES";
                FindObjectOfType<SendedListUI>().UpdateStats(item);
            }
        });
        
        Save();
    }

    public void SendAllToFile()
    {
        string direction = FileBrowser.SaveFile("Log", ".csv");

           using (StreamWriter sw = File.CreateText(direction)) {
                string finalString = "";
                for (int i = 0; i < reportHeaders.Length; i++) {
                    if (finalString != "") {
                        finalString += reportSeparator;
                    }
                    finalString += reportHeaders[i];
                }
                sw.WriteLine(finalString);
        }
        
        using (StreamWriter sw = File.AppendText(direction))
        {
            _itemList._sendedItems.ForEach(item =>
            {
                string finalString = "";
                finalString += item._method;
                finalString += reportSeparator;
                finalString += item._login;
                finalString += reportSeparator;
                finalString += item._sended;
                finalString += reportSeparator;
                finalString += item._fileName;
                finalString += reportSeparator;
                finalString += item._time;
                sw.WriteLine(finalString);
            });
       
        }
    }

    public async void SendNoSended()
    {
        Debug.Log("Начало метода FactorialAsync"); // выполняется синхронно
        await Task.Run(()=>SendNoSendedHandle());                // выполняется асинхронно
        Debug.Log("Конец метода FactorialAsync");
    }

    public async void SendNoSendedHandle()
    {
       // await _itemList._sendedItems.ForEachAsync(async item => );
       for (int x = 0;  x < _itemList._sendedItems.Count; x++)
       {
           
            var item = _itemList._sendedItems[x];
            if (item._sended == "NO")
            {
                Debug.Log("RESEND");
                
                _dispatcher.Enqueue(() =>
                {
                    var items = FindObjectsOfType<SendedListItemUI>();

                    for (int i = 0; i < items.Length; i++)
                    {
                        if(items[i].SendedItem == item)
                            items[i].ResendAnim();
                    }

                });
               
                if (item._method == "Mail")
                {
                    await _mailSendManager.ResendEmail(item._login,item._path,item._fileName, item._time);
                }
                else
                {
                    string fileExtension = Path.GetExtension(item._path);
     
                    if(fileExtension == ".mov" || fileExtension == ".mp4")
                        await _instagramManager.ReSendDirectImage(item._login,item._path,item._time,item._fileName);
                    else await _instagramManager.ReSendDirectImage(item._login,item._path,item._time,item._fileName);
                }
            }
        }
    }

    public void ClearList()
    {
        _itemList._sendedItems.Clear();
        FindObjectOfType<SendedListUI>().Clear();
        Save();
    }

    private void Save()
    {
        PlayerPrefs.SetString("ItemList", JsonUtility.ToJson(_itemList));
        PlayerPrefs.Save();
    }
}

[System.Serializable]
public class SendedItemsList
{
    public List<SendedItem> _sendedItems = new List<SendedItem>();
}


[System.Serializable]
public class SendedItem
{   
    public string _method;

    public string _login;

    public string _sended;

    public string _fileName;

    public string _time;

    public string _path;

    public SendedItem(string method, string login, string sended, string fileName, string time, string path)
    {
        _method = method;
        _login = login;
        _sended = sended;
        _fileName = fileName;
        _time = time;
        _path = path;
    }
}
