﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SendedListUI : MonoBehaviour
{
    [SerializeField] private GameObject _sendedListItemPRefab;

    [SerializeField] private Transform _sendedListParretn;

    private List<UiSendedItemData> _uiItems = new List<UiSendedItemData>();

    private int _startId = 0;

    [SerializeField] private Text _sendProcessing;

    [SerializeField] private Text _sendProcessingInGallery;

    [SerializeField] private Text _sendProcessingInLeftPanel;
    
    private int sendedCount;

    public void UiUpdate(SendedItemsList sendedList)
    {
        sendedList._sendedItems.ForEach(item =>
        {
            var sendedItem = Instantiate(_sendedListItemPRefab, _sendedListParretn).GetComponent<SendedListItemUI>();
            sendedItem.SetUp(item, _startId);
            _uiItems.Add(new UiSendedItemData(_startId,sendedItem.gameObject));
            _startId++;
        });

        SendProcessingUiUpdate();
    }

    private void SendProcessingUiUpdate()
    {
        sendedCount = 0;

        _uiItems.ForEach(item =>
        {
            var uiItem = item._item.GetComponent<SendedListItemUI>().SendedItem;
            if (uiItem._sended == "YES")
                sendedCount++;
        });

        TextUpdate();
    }

    public void TextUpdate()
    {
        if (LocalizationManager._instance.Language == Language.ru)
        {
            _sendProcessing.text = "ИДЕТ ОТПРАВКА " + sendedCount + " ИЗ " + _uiItems.Count;
            _sendProcessingInGallery.text = "ИДЕТ ОТПРАВКА " + sendedCount + " ИЗ " + _uiItems.Count;
            _sendProcessingInLeftPanel.text = "ИДЕТ ОТПРАВКА " + sendedCount + " ИЗ " + _uiItems.Count;
            
        }
        else
        {
            _sendProcessing.text = "SENDING " + sendedCount + " OF " + _uiItems.Count;
            _sendProcessingInGallery.text ="SENDING " + sendedCount + " OF " + _uiItems.Count;
            _sendProcessingInLeftPanel.text = "SENDING " + sendedCount + " OF " + _uiItems.Count;
        }
        
    }


    public void Delete(int id)
    {
       var selectedItem = _uiItems.Find(x => { return x._id == id; });

        if (selectedItem != null)
        {
            Destroy(selectedItem._item);
            _uiItems.Remove(selectedItem);
        }
    }

    public void UpdateStats(SendedItem sendedItem)
    {
        _uiItems.ForEach(item =>
        {
            var uiItem = item._item.GetComponent<SendedListItemUI>().SendedItem;

            if (uiItem._login == sendedItem._login && uiItem._method == sendedItem._method &&
                uiItem._fileName == sendedItem._fileName && uiItem._time == sendedItem._time)
            {
                item._item.GetComponent<SendedListItemUI>().UpdateStatus();
            }
        });
        
        SendProcessingUiUpdate();
        
    }

    public void Clear()
    {
        _uiItems.ForEach(item =>
        {
            Destroy(item._item);
        });
        
        _uiItems.Clear();

        sendedCount = 0;
        
        TextUpdate();
    }

    public void Add(SendedItem item)
    {
        var sendedItem = Instantiate(_sendedListItemPRefab, _sendedListParretn).GetComponent<SendedListItemUI>();
        sendedItem.SetUp(item,_startId);
        sendedItem.ResendAnim();
        _uiItems.Add(new UiSendedItemData(_startId,sendedItem.gameObject));
        TextUpdate();
        _startId++;
        SendProcessingUiUpdate();
    }
}

[System.Serializable]
public class UiSendedItemData
{
    public int _id;

    public GameObject _item;

    public UiSendedItemData(int id, GameObject item)
    {
        _id = id;
        _item = item;
    }
}
