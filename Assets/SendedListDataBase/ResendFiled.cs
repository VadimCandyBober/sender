﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResendFiled : MonoBehaviour
{
    private InputField _timeField;

    private void Awake()
    {
        _timeField = GetComponent<InputField>();
    }
    public void SetTime()
    {
        try
        {
            ResendAllTickRate._instance.SetTime(System.Convert.ToInt32(_timeField.text));
        }
        catch (Exception e)
        {
            
        }
       
    }
}
