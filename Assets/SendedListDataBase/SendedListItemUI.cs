﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Threading.Tasks;
using Dropbox.Api.Files;

public class SendedListItemUI : MonoBehaviour
{
   [SerializeField] private Text _method;

   [SerializeField] private Text _login;

   [SerializeField] private Text _sended;

   [SerializeField] private Text _fileName;

    [SerializeField] private Text _time;

    private SendedItem _sendedItem;

    [SerializeField] private RectTransform _resendImage;

    public SendedItem SendedItem => _sendedItem;

    private bool _resended;

    private int _id;

    private MailSendManager _mailManager;

    private InstagramManager _instagramManager;

    private SmsManager _smsManager;

    private void Awake()
    {
        _mailManager = FindObjectOfType<MailSendManager>();
        _instagramManager = FindObjectOfType<InstagramManager>();
        _smsManager = FindObjectOfType<SmsManager>();
    }
   
   public void SetUp(SendedItem sendedItem, int id)
   {
      _method.text = sendedItem._method;

      _login.text = sendedItem._login;

      _sended.text = sendedItem._sended;

       _fileName.text = sendedItem._fileName;

       _sendedItem = sendedItem;

       _time.text = sendedItem._time;

       _id = id;
   }

    public void Delete()
    {
       // FindObjectOfType<SendedListUI>().Delete(_id);
    }

    public void ResendAnim()
    {
        _resended = true;
        try
        {   
            if(gameObject.active)
                StartCoroutine(Delay());
        }
        catch (Exception e)
        {
            Debug.Log(e);  
        }
     
    }
    
    public void OffAnim()
    {
        _resended = false;
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(180);
        
        OffAnim();
    }
    
    

    public async void ReseSend()
    {
        Debug.Log("Начало метода FactorialAsync"); // выполняется синхронно
        await Task.Run(()=>ResendHandle());                // выполняется асинхронно
        Debug.Log("Конец метода FactorialAsync");
        
    }

    public async void ResendHandle()
    {
        _resended = true;

        if (_sendedItem._method == "Mail")
        {
            await _mailManager.ResendEmail(_sendedItem._login,_sendedItem._path,_sendedItem._fileName, _sendedItem._time);
        }
        else if(_sendedItem._method == "Instagram")
        {
            string fileExtension = Path.GetExtension(_sendedItem._path);
     
            if(fileExtension == ".mov" || fileExtension == ".mp4")
                await _instagramManager.ReSendDirectImage(_sendedItem._login,_sendedItem._path,_sendedItem._time,_sendedItem._fileName);
            else await _instagramManager.ReSendDirectImage(_sendedItem._login,_sendedItem._path,_sendedItem._time,_sendedItem._fileName);
        } else if(_sendedItem._method == "SMS")
        {
            await _smsManager.ResendSms(_sendedItem._login,_sendedItem._path,_sendedItem._time,_sendedItem._fileName);
        }
    }

    private void Update()
    {
        if(_resended)
            _resendImage.localEulerAngles += new Vector3(0,0,1);
    }


    public void UpdateStatus()
    {
        _resended = false;
        _resendImage.localEulerAngles = new Vector3(0,0,0);
        _sended.text = "YES";
    }
   
}
