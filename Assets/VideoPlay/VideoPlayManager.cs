﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using RenderHeads.Media.AVProVideo;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoPlayManager : MonoBehaviour
{
    [SerializeField] private GameObject _allFiles;

    [SerializeField] private GameObject _videoUI;

    [SerializeField] private VideoPlayer _player;

    [SerializeField] private RawImage _videoImage;

    public static VideoPlayManager _instance;

    [SerializeField] private Transform _screenObj;

    [SerializeField] private GameObject _backGround;

    [SerializeField] private MediaPlayer _mediaPlayer;

    [SerializeField] private GameObject _playUI;

    [SerializeField] private Image _imagePreview;

    [SerializeField] private GameObject _videoDisplay;
    
    [SerializeField] private RawImage _gifPDisplay;
    
    [SerializeField]private UniGifImage m_uniGifImage;

    [SerializeField] private Texture2D _loading;
 
    private bool _videoOpened;
    
    private string _nowPath;

    private bool _isVideoPlayed;

    private void Awake()
    {
        _instance = this;
        
    }

    public void Open(string path, Sprite image, PreviewObject previewObject)
    {
        _imagePreview.enabled = false;
        
        _videoDisplay.SetActive(true);

        _gifPDisplay.gameObject.SetActive(false); 

        _allFiles.SetActive(false);
        
        _videoUI.SetActive(true);
        
        _playUI.SetActive( true);

        StartCoroutine(VideoPLay(path, image, previewObject));
    }
    
    public void OpenImage(string path, Sprite image, PreviewObject previewObject, Sprite viewImage)
    {
        _playUI.SetActive(false);
        
        _allFiles.SetActive(false);
        
        _gifPDisplay.gameObject.SetActive(false); 
        
        _videoDisplay.SetActive(false);
        
        _videoUI.SetActive(true);

        FolderOpen._instance.StopAll();
        
        _nowPath = path;
        
        if(AddVideoToSendList._instance.CanAdd())
            AddVideoToSendList._instance.Add(path,image,previewObject, 0);

        _imagePreview.enabled = true;

        _imagePreview.sprite = viewImage;

        float koff = viewImage.rect.width / viewImage.rect.height;

        _imagePreview.transform.localScale = new Vector3(0.8f * koff, 0.8f, 1);
        
    }
    
    public void OpenGif(string path, Sprite image, PreviewObject previewObject, Sprite viewImage)
    {
        _playUI.SetActive(false);
        
        _allFiles.SetActive(false);

        _imagePreview.enabled = false;
        
        _gifPDisplay.gameObject.SetActive(true);

        _gifPDisplay.texture = _loading;

        _gifPDisplay.color = UnityEngine.Color.black;

        _videoDisplay.SetActive(false);
        
        _videoUI.SetActive(true);

        FolderOpen._instance.StopAll();
        
        _nowPath = path;
        
        if(AddVideoToSendList._instance.CanAdd())
            AddVideoToSendList._instance.Add(path,image,previewObject, 0);

        StartCoroutine(GifPlay(path));

    }

    IEnumerator GifPlay(string path)
    {
        Debug.Log(path);
        yield return StartCoroutine(m_uniGifImage.SetGifFromUrlCoroutine(path));
    }

    IEnumerator VideoPLay(string path, Sprite image, PreviewObject previewObject)
    { 
        FolderOpen._instance.StopAll();

        _videoOpened = true;
        
        _mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, path,false);

        _mediaPlayer.Stop();

        _nowPath = path;

        yield return new WaitForSeconds(0.1f);

        _isVideoPlayed = false;
        
        _playUI.SetActive(!_isVideoPlayed);
        
        if(AddVideoToSendList._instance.CanAdd())
            AddVideoToSendList._instance.Add(path,image,previewObject, 0);
    }

    public void PlayVideo()
    {
        _isVideoPlayed = !_isVideoPlayed;
        
        _playUI.SetActive(!_isVideoPlayed);
        
        if(_isVideoPlayed)
            _mediaPlayer.Play();
        else _mediaPlayer.Stop();
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0) && _isVideoPlayed)
            PlayVideo();
    }
    

    public void Close()
    {
        _backGround.SetActive(true);
        try
        {
            _mediaPlayer.Stop();
            _isVideoPlayed = false;
            AddVideoToSendList._instance.RemoveFileToSend(_nowPath);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
        
        
        _allFiles.SetActive(true);
        
        _videoUI.SetActive(false);
        
        if (_videoOpened)
        {
            FolderOpen._instance.RestartALl();
            Debug.Log("RESTART");
        }
          
        _videoOpened = false;
    }
    
    
}
