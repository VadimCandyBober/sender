﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Dropbox.Api.Sharing;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using UnityEngine;
using UnityEngine.UI;

public class SmsManager : MonoBehaviour
{
    public static SmsManager _instance; 
    
    [SerializeField] private InputField _sidInput;

    [SerializeField] private InputField _tokenInput;

    [SerializeField] private InputField _numberInput;

    [SerializeField] private InputField _mailBody;
    
    private SmsProperties _smsProperties;
    
    private UnityMainThreadDispatcher _Dispatcher;

    private GoogleDriveTest _googleDriveTest;

    private void Awake()
    {
        _instance = this;
        _Dispatcher = FindObjectOfType<UnityMainThreadDispatcher>();
        _googleDriveTest = FindObjectOfType<GoogleDriveTest>();
        SmsPropertiesGet();
        UiSetUp();
    }

    public void SmsPropertiesGet()
    {
        var json = PlayerPrefs.GetString("Sms");

        if (!String.IsNullOrEmpty(json))
        {
            _smsProperties = JsonUtility.FromJson<SmsProperties>(json);
        }
        else
        {
            _smsProperties = new SmsProperties();
        }
    }

    public void SetSid()
    {
        _smsProperties.accountSid = _sidInput.text;
        SaveProperties();
    }

    public void SetToken()
    {
        _smsProperties.authToken = _tokenInput.text;
        SaveProperties();
    }

    public void SetNumber()
    {
        _smsProperties.phoneNumber = _numberInput.text;
        SaveProperties();
    }

    public void SetMailBody()
    {
        _smsProperties.body = _mailBody.text;
        SaveProperties();
    }

    public void SaveProperties()
    {
        PlayerPrefs.SetString("Sms",JsonUtility.ToJson(_smsProperties));
        PlayerPrefs.Save();
    }

    private void UiSetUp()
    {
        _sidInput.text = _smsProperties.accountSid;
        _tokenInput.text = _smsProperties.authToken;
        _numberInput.text = _smsProperties.phoneNumber;
        _mailBody.text = _smsProperties.body;
    }

    public async Task ResendSms(string to, string path, string time, string fileName)
    {
        TwilioClient.Init(_smsProperties.accountSid, _smsProperties.authToken); 
        
        string url = await _googleDriveTest.GoogleDriveUploadToSms(path);

        if (url != "-1")
        {
            try
            {
                var message = MessageResource.Create
                (
                    body: _mailBody.text + " " + url,
                    from: new Twilio.Types.PhoneNumber(_smsProperties.phoneNumber),
                    to: new Twilio.Types.PhoneNumber(to)
                );

            }
            catch (Exception e)
            {
                Debug.Log(e);
                return;
            }
        }
       
        _Dispatcher.Enqueue(() =>
        {
            SendedList._instance.UpdateStat("SMS", to, fileName, time);
        });
    }
    
    public async Task SendSms(string to, string path)
    {
        string fileName = Path.GetFileName(path);
        
        string time = DateTime.Now.ToString();

        _Dispatcher.Enqueue(() =>
        {
            SendedList._instance.AddToItemList(
                new SendedItem("SMS", to, "NO", fileName, time, path));
        });
        
       TwilioClient.Init(_smsProperties.accountSid, _smsProperties.authToken); 
        
        string url = await _googleDriveTest.GoogleDriveUploadToSms(path);

        if (url != "-1")
        {
            try
            {
                var message = MessageResource.Create
                (
                    body: _mailBody.text + " " + url,
                    from: new Twilio.Types.PhoneNumber(_smsProperties.phoneNumber),
                    to: new Twilio.Types.PhoneNumber(to)
                );

            }
            catch (Exception e)
            {
                Debug.Log(e);
                return;
            }
        }
       
        _Dispatcher.Enqueue(() =>
        {
            SendedList._instance.UpdateStat("SMS", to, fileName, time);
        });
    }
}

[System.Serializable]
public class SmsProperties
{
    public string accountSid = "";
    
    public string authToken = "";

    public string phoneNumber = "";

    public string body = "";
}
